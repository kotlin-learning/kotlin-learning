package com.ecovacs.commonlib.imgeload

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Size
import android.widget.ImageView
import androidx.annotation.DrawableRes
import coil.ImageLoader
import coil.memory.MemoryCache
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory

/**
 * 用于加载图片的扩展类
 * 需求：
 * 可以加载Coil Glide ,
 * 只初始化一次ImageLoader用于加载不同的图片，
 * 可以通过ImageRequest设置不同的参数
 * @author lifeng.xing
 *
 * create on 2022/3/8
 */
/**
 *
 * @receiver ImageView
 * @param data Any?  图片的数据源
 * @param requestLib ImageRequestLib 请求参数
 * @param isCoil Boolean 是否是Coil 默认为Coil
 */
@SuppressLint("CheckResult", "RestrictedApi")
inline fun ImageView.loadImg(
    data: Any?,
    requestLib: ImageRequestLib,
    isCoil: Boolean = true
) {
    if (isCoil) {
        loadByCoil(this, data, requestLib)
    } else {
        loadByGlide(this, data, requestLib)
    }
}

/**
 * 使用Glide加载
 * @param imageView ImageView
 * @param data Any?
 * @param requestLib ImageRequestLib
 */
fun loadByCoil(imageView: ImageView, data: Any?, requestLib: ImageRequestLib) {
    //ImageRequest
    val builder = ImageRequest.Builder(imageView.context)
    builder.apply {
        //placeholder
        if (requestLib.placeholderResId != -1) {
            placeholder(requestLib.placeholderResId)
        } else {
            placeholder(requestLib.placeholderDrawable)
        }
        //errorholder
        if (requestLib.errorResId != -1) {
            placeholder(requestLib.errorResId)
        } else {
            placeholder(requestLib.errorDrawable)
        }
        //fallback
        if (requestLib.fallbackResId != -1) {
            fallback(requestLib.fallbackResId)
        } else {
            fallback(requestLib.fallbackDrawable)
        }
        //渐隐
        if (requestLib.isCrossFade) {
            //coil 直接设置时间即可
            crossfade(requestLib.crossFadeduration)
        }
        //大小 px
        requestLib.size?.let {
            this.size(it.width, it.height)
        }
        //内存缓存策略
        memoryCachePolicy(CachePolicy.ENABLED)
        //磁盘缓存策略 需要磁盘读写权限
        diskCachePolicy(CachePolicy.ENABLED)


    }
    val request = builder.data(data)
        .target(imageView)
        .build()
    //请求加载图片
    CoilIamgeLoader.getImageLoader(imageView.context).enqueue(request)
}

/**
 * 使用Glide加载
 * @param imageView ImageView
 * @param data Any?
 * @param requestLib ImageRequestLib
 */
@SuppressLint("CheckResult")
fun loadByGlide(imageView: ImageView, data: Any?, requestLib: ImageRequestLib) {
    val glideBuilder = Glide.with(imageView).load(data)
    glideBuilder.apply {
        //占位图
        if (requestLib.placeholderResId != -1) {
            placeholder(requestLib.placeholderResId)
        } else {
            placeholder(requestLib.placeholderDrawable)
        }
        //errorholder
        if (requestLib.errorResId != -1) {
            placeholder(requestLib.errorResId)
        } else {
            placeholder(requestLib.errorDrawable)
        }
        //fallback
        if (requestLib.fallbackResId != -1) {
            fallback(requestLib.fallbackResId)
        } else {
            fallback(requestLib.fallbackDrawable)
        }
        //center crop 加载
        centerCrop()
        //渐隐
        if (requestLib.isCrossFade) {
            //Glide需要DrawableCrossFadeFactory实现
            val factory = DrawableCrossFadeFactory.Builder(requestLib.crossFadeduration)
                .setCrossFadeEnabled(true)
                .build()
            transition(DrawableTransitionOptions.withCrossFade(factory))
        }
        //大小 px
        requestLib.size?.let {
            this.override(it.width, it.height)
        }
    }
    glideBuilder.into(imageView)
}

/**
 * Coil iamge loader
 * 全局获取Coil ImageLoader的单例
 * @constructor Create empty Coil iamge loader
 */
object CoilIamgeLoader {
    lateinit var imageLoader: ImageLoader

    //获取imageloader
    fun getImageLoader(context: Context): ImageLoader {
        //未初始化 初始化
        if (!::imageLoader.isInitialized) {
            imageLoader = ImageLoader.Builder(context)
                .memoryCache(MemoryCache.Builder(context).maxSizePercent(0.25).build())
                .crossfade(true)
                .build()
        }
        return imageLoader
    }
}

/**
 * Builder
 * 用于初始化参数
 * 1、缓存，
 * 2、占位
 * 3、图片缩放centerCrop
 * 4、Gif
 * 5、Size加载大小
 * 6、transformations()
 * BlurTransformation	高斯模糊
 * CircleCropTransformation	圆形裁剪
 * GrayscaleTransformation	图片置灰
 * RoundedCornersTransformation	添加圆角
 * @constructor Create empty Builder
 */
class ImageRequestLib(
    val context: Context,
    //占位图
    val placeholderResId: Int,
    val placeholderDrawable: Drawable?,
    //错误图
    val errorResId: Int,
    val errorDrawable: Drawable?,
    //请求url为空 图片
    val fallbackResId: Int,
    val fallbackDrawable: Drawable?,
    //crossfade 是否渐隐以及时间默认300
    val isCrossFade: Boolean,
    val crossFadeduration: Int = 300,
    //重载的图片大小
    val size: Size? = null
) {
    /**
     * 传入builder的构造方法
     * @param builder Builder
     * @constructor
     */
    private constructor(builder: Builder) : this(
        context = builder.context,
        placeholderDrawable = builder.placeholderDrawable,
        placeholderResId = builder.placeholderResId,
        errorResId = builder.errorResId,
        errorDrawable = builder.errorDrawable,
        fallbackResId = builder.fallbackResId,
        fallbackDrawable = builder.fallbackDrawable,
        isCrossFade = builder.isCrossFade,
        crossFadeduration = builder.crossFadeduration,
        size = builder.size
    )

    class Builder {

        constructor(context: Context) {
            this.context = context
        }

        constructor(context: Context, request: ImageRequestLib) {
            this.context = context
            placeholderResId = request.placeholderResId
            placeholderDrawable = request.placeholderDrawable
            errorResId = request.errorResId
            errorDrawable = request.errorDrawable
            fallbackResId = request.fallbackResId
            fallbackDrawable = request.fallbackDrawable
            isCrossFade = request.isCrossFade
            crossFadeduration = request.crossFadeduration
            size = request.size
        }

        var context: Context
            private set

        @DrawableRes
        var placeholderResId: Int = -1
            private set
        var placeholderDrawable: Drawable? = null
            private set

        @DrawableRes
        var errorResId: Int = -1
            private set
        var errorDrawable: Drawable? = null
            private set

        @DrawableRes
        var fallbackResId: Int = -1
            private set
        var fallbackDrawable: Drawable? = null
            private set

        //渐隐效果和时间
        var isCrossFade: Boolean = true
            private set
        var crossFadeduration: Int = 300
            private set

        //重载图片的大小
        var size: Size? = null
            private set

        fun placeholder(@DrawableRes placeholderResId: Int) = apply {
            this.placeholderResId = placeholderResId
        }

        fun placeholder(placeholderDrawable: Drawable) = apply {
            this.placeholderDrawable = placeholderDrawable
        }

        fun error(@DrawableRes errorResId: Int) = apply {
            this.errorResId = errorResId
        }

        fun error(errorDrawable: Drawable) = apply {
            this.errorDrawable = errorDrawable
        }

        fun fallback(@DrawableRes errorResId: Int) = apply {
            this.fallbackResId = errorResId
        }

        fun fallback(errorDrawable: Drawable) = apply {
            this.fallbackDrawable = errorDrawable
        }

        fun setCrossFade(isCrossFade: Boolean) = apply {
            this.isCrossFade = isCrossFade
        }

        fun setCrossFadeDuration(crossFadeduration: Int) = apply {
            this.crossFadeduration = crossFadeduration
        }

        fun setSize(size: Size) = apply {
            this.size = size
        }

        fun setSize(width: Int, height: Int) = apply {
            this.size = Size(width, height)
        }

        /**
         * 创建ImageRequest 用于请求图片
         * @return ImageRequest
         */
        fun build(): ImageRequestLib {
            return ImageRequestLib(this)
        }
    }
}
