package com.timi.kotlincore.store

import android.content.Context

/**
 * @ClassName: 定义给外部调用的接口方法
 * @Description:
 * @author: lifeng xing
 * @date: 2022/8/4
 */
interface IStore {
    /**
     * 切换存储方式
     */
    fun swichStore(context: Context, storeType: StoreType): AStore

    /**
     * 初始化
     * @param context
     */
    fun init(context: Context): AStore

    /**
     * 初始化
     * @param context
     * @param config
     */
    fun init(context: Context, config: StoreConfig): AStore

    /**
     * 获取之前的配置
     * @return StoreConfig
     */
    fun getConfig(): StoreConfig

    /**
     * save value 保存所有类型的方法
     * @param key
     * @param value
     */
    fun save(key: String, value: Any): AStore

    /**
     * remove key 删除XXX key
     * @param key
     */
    fun remove(key: String): AStore

    /**
     * remove key 删除XXX key
     * @param keys
     */
    fun removeKeys(keys: List<String>): AStore

    /**
     * 获取所有的数据(无论是mmkv还是sp 或者是其他)
     */
    fun getAll(): MutableMap<String, *>

    /**
     * 是否包含key
     */
    fun isContainKey(key: String): Boolean

    /**
     *  清除所有数据
     */
    fun clearAll()

    /**
     * 迁移的方法
     * @param values key和value的map
     */
    fun migration(values: MutableMap<String, *>)

    /**
     * 获取字符串
     * @param key key
     * @param default 默认值
     * @return String
     */
    fun getString(key: String, default: String = ""): String

    /**
     * 获取Long
     * @param key key
     * @param default 默认值
     * @return Long
     */
    fun getLong(key: String, default: Long = 0L): Long

    /**
     * 获取Boolean
     * @param key key
     * @param default 默认值
     * @return Boolean
     */
    fun getBoolean(key: String, default: Boolean = false): Boolean

    /**
     * 获取Float
     * @param key key
     * @param default 默认值
     * @return Float
     */
    fun getFloat(key: String, default: Float = 0f): Float

    /**
     * 获取Int
     * @param key key
     * @param default 默认值
     * @return Int
     */
    fun getInt(key: String, default: Int = 0): Int

    /**
     * 获取Double
     * @param key key
     * @param default 默认值
     * @return Double
     */
    fun getDouble(key: String, default: Double = 0.0): Double
}
