package com.timi.kotlincore.store

import android.content.Context

interface ISaveKVFactory {
    fun getISaveKVImp(contextStore: Context, storeConfig: StoreConfig): AStore
}

/**
 * 存储类型
 * SharePre SharePreference 存储
 * MMKV       mmkv 存储
 * DataStore jetpack组件
 */
enum class StoreType {
    SharePre, MMKV, DataStore
}

/**
 * 日志界别
 */
enum class StoreLogLevel {
    LevelDebug,
    LevelInfo,
    LevelWarning,
    LevelError,
    LevelNone,
}
