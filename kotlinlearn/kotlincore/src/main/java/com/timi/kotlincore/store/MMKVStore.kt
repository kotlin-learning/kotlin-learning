package com.timi.kotlincore.store

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.tencent.mmkv.MMKV
import com.tencent.mmkv.MMKV.initialize
import java.lang.Exception

/**
 * @ClassName: MMKVManager
 * @Description: 用于处理MMKV 初始化，存储数据
 * @author: lifeng xing
 * @date: 2022/8/2
 */
class MMKVStore(private val config: StoreConfig) : AStore() {
    /**
     * tag
     */
    private val TAG = MMKVStore::class.simpleName

    /**
     * 获取字符串
     * @param key key
     * @param default 默认值
     * @return String
     */
    override fun getString(key: String, default: String): String {
        val kv = MMKV.defaultMMKV()
        kv.decodeString(key, default)?.let {
            return kv.decodeString(key, default)!!
        }
        return config.defaultString
    }

    /**
     * 获取Long
     * @param key key
     * @param default 默认值
     * @return Long
     */
    override fun getLong(key: String, default: Long): Long {
        val kv = MMKV.defaultMMKV()
        return kv.decodeLong(key, default)
    }

    /**
     * 获取Boolean
     * @param key key
     * @param default 默认值
     * @return Boolean
     */
    override fun getBoolean(key: String, default: Boolean): Boolean {
        val kv = MMKV.defaultMMKV()
        return kv.decodeBool(key, default)
    }

    /**
     * 获取Float
     * @param key key
     * @param default 默认值
     * @return Float
     */
    override fun getFloat(key: String, default: Float): Float {
        val kv = MMKV.defaultMMKV()
        return kv.decodeFloat(key, default)
    }

    /**
     * 获取Int
     * @param key key
     * @param default 默认值
     * @return Int
     */
    override fun getInt(key: String, default: Int): Int {
        val kv = MMKV.defaultMMKV()
        return kv.decodeInt(key, default)
    }

    /**
     * 获取Double
     * @param key key
     * @param default 默认值
     * @return Double
     */
    override fun getDouble(key: String, default: Double): Double {
        val kv = MMKV.defaultMMKV()
        return kv.decodeDouble(key, default)
    }

    /**
     * 是否包含XX key
     * @param key key
     * @return Boolean 布尔
     */
    override fun isContainKey(key: String): Boolean {
        val kv = MMKV.defaultMMKV()
        return kv.containsKey(key)
    }

    override fun remove(key: String) {
        removeKeys(listOf(getRealKey(key)))
    }

    override fun removeKeys(keys: List<String>) {
        val kv = MMKV.defaultMMKV()
        keys.forEach {
            kv.removeValueForKey(it)
        }
    }
    /**MMKV提供了多种初始化方法*/
    /**
     * 初始化MMKV 尽量在APPlication中初始化
     * @param ctx
     */
    override fun init(context: Context): AStore {
        return init(context, StoreConfig())
    }

    override fun init(context: Context, config: StoreConfig): AStore {
        // 路径
        if (TextUtils.isEmpty(config.rootDir)) {
            initialize(context)
        } else {
            initialize(context, config.rootDir)
        }
        return this
    }

    override fun getConfig(): StoreConfig {
        return config
    }

    /**
     * 存储数据
     * @param key key
     * @param value 数据
     */
    override fun save(key: String, value: Any) {
        try {
            val kv = MMKV.defaultMMKV()
            val realKey = getRealKey(key, value)
            when (value) {
                // 存储字符串
                is String -> {
                    kv.encode(realKey, value)
                }
                // 存储Boolean
                is Boolean -> {
                    kv.encode(realKey, value)
                }
                // 存储Int
                is Int -> {
                    kv.encode(realKey, value)
                }
                // 存储Float
                is Float -> {
                    kv.encode(realKey, value)
                }
                // 存储Double
                is Double -> {
                    kv.encode(realKey, value)
                }
                // 存储Long
                is Long -> {
                    kv.encode(realKey, value)
                }
            }
        } catch (e: Exception) {
            if (config.logLevel == StoreLogLevel.LevelError) {
                Log.e(TAG, "save error：${e.message}")
            }
        }
    }

    /**
     * 获取所有存储的key和value
     * @return 所有的key：value
     */
    override fun getAll(): MutableMap<String, *> {
        val kv = MMKV.defaultMMKV()
        // 所有的key
        val keys = kv.allKeys()
        val map = mutableMapOf<String, Any>()
        if (keys != null) {
            for (it in keys) {
                if (it.contains("@")) {
                    val typeList = it.split("@")
                    when (typeList[typeList.size - 1]) {
                        String::class.simpleName -> map[it] = getString(it, "")
                        Int::class.simpleName -> map[it] = getInt(it, 0)
                        Long::class.simpleName -> map[it] = getLong(it, 0L)
                        Float::class.simpleName -> map[it] = getFloat(it, 0f)
                        Double::class.simpleName -> map[it] = getDouble(it, 0.0)
                        Boolean::class.simpleName -> map[it] = getBoolean(it, false)
                    }
                }
            }
        }
        return map
    }

    /**
     * 通过value获取真实的key
     * @param key
     */
    private fun getRealKey(key: String, value: Any): String {
        when (value) {
            is String -> return getTypeKey<String>(key)
            is Long -> return getTypeKey<Long>(key)
            is Double -> return getTypeKey<Double>(key)
            is Float -> return getTypeKey<Float>(key)
            is Int -> return getTypeKey<Int>(key)
            is Boolean -> return getTypeKey<Boolean>(key)
        }
        return getTypeKey<String>(key)
    }

    /**
     * 获取真实的key前提是已经存储过当前的key
     * @param key
     */
    private fun getRealKey(key: String): String {
        val kv = MMKV.defaultMMKV()
        val typeKys = listOf(
            getTypeKey<String>(key),
            getTypeKey<Long>(key),
            getTypeKey<Double>(key),
            getTypeKey<Float>(key),
            getTypeKey<Int>(key),
            getTypeKey<Boolean>(key)
        )
        typeKys.forEach {
            if (kv.containsKey(it)) {
                return it
            }
        }
        return getTypeKey<String>(key)
    }

    /**
     * 将key 以key+@+simpleName的形式做为key
     * @param key
     */
    private inline fun <reified T> getTypeKey(key: String): String {
        val type = "@" + T::class.simpleName
        return if (key.contains(type)) {
            key
        } else {
            key + type
        }
    }

    /**
     * 清空存储
     */
    override fun clearAll() {
        val kv = MMKV.defaultMMKV()
        kv.clearAll()
    }

    /**
     * 迁移 直接通过map的形式将所有数据迁移出来
     */
    override fun migration(values: MutableMap<String, *>) {
        values.forEach {
            it.value?.let { it1 -> save(it.key, it1) }
        }
    }
}
