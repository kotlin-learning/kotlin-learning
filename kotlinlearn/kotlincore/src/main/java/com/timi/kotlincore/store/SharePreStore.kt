package com.timi.kotlincore.store

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import java.lang.Exception

/**
 * @ClassName: SharePre
 * @Description: Sharepreference
 * @author: lifeng xing
 * @date: 2022/8/3
 */
class SharePreStore(private val config: StoreConfig) : AStore() {
    private val TAG = SharePreStore::class.simpleName

    /**
     * SharedPreferences
     */
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    /**
     * init
     */
    override fun init(context: Context): AStore {
        return init(context, StoreConfig())
    }

    override fun init(context: Context, config: StoreConfig): AStore {
        sharedPreferences = context.getSharedPreferences(config.sharePreName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        return this
    }

    override fun getConfig(): StoreConfig {
        return config
    }

    override fun save(key: String, value: Any) {
        try {
            when (value) {
                // 存储字符串
                is String -> {
                    editor.putString(key, value)
                }
                // 存储Boolean
                is Boolean -> {
                    editor.putBoolean(key, value)
                }
                // 存储Int
                is Int -> {
                    editor.putInt(key, value)
                }
                // 存储Float
                is Float -> {
                    editor.putFloat(key, value)
                }
                // 存储Double
                is Double -> {
                    editor.putLong(key, value.toRawBits())
                }
                // 存储Long
                is Long -> {
                    editor.putLong(key, value)
                }
            }
            editor.commit()
        } catch (e: Exception) {
            if (config.logLevel == StoreLogLevel.LevelError) {
                Log.e(TAG, "save error：${e.message}")
            }
        }
    }

    override fun remove(key: String) {
        editor.remove(key)
    }

    override fun removeKeys(keys: List<String>) {
        keys.forEach {
            editor.remove(it)
        }
    }

    override fun getAll(): MutableMap<String, *> {
        return sharedPreferences.all
    }

    override fun isContainKey(key: String): Boolean {
        return sharedPreferences.contains(key)
    }

    override fun clearAll() {
        editor.clear()
        editor.commit()
    }

    override fun migration(values: MutableMap<String, *>) {
        values.forEach {
            when (it.value) {
                is String -> save(it.key, it.value as String)
                is Float -> save(it.key, it.value as Float)
                is Long -> save(it.key, it.value as Long)
                is Boolean -> save(it.key, it.value as Boolean)
                is Double -> save(it.key, it.value as Double)
                is Int -> save(it.key, it.value as Int)
            }
        }
    }

    override fun getString(key: String, default: String): String {
        return sharedPreferences.getString(key, default).toString()
    }

    override fun getLong(key: String, default: Long): Long {
        return sharedPreferences.getLong(key, default)
    }

    override fun getBoolean(key: String, default: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, default)
    }

    override fun getFloat(key: String, default: Float): Float {
        return sharedPreferences.getFloat(key, default)
    }

    override fun getInt(key: String, default: Int): Int {
        return sharedPreferences.getInt(key, default)
    }

    override fun getDouble(key: String, default: Double): Double {
        return Double.fromBits(sharedPreferences.getLong(key, default.toRawBits()))
    }
}
