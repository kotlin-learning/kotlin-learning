package com.timi.kotlincore.store

import android.content.Context

/**
 * @ClassName: ISaveKV
 * @Description:  定义统一的抽象方法
 * @author: lifeng xing
 * @date: 2022/8/2
 */
abstract class AStore {
    /**
     * 初始化
     * @param context
     */
    abstract fun init(context: Context): AStore

    /**
     * 初始化
     * @param context
     * @param config
     */
    abstract fun init(context: Context, config: StoreConfig): AStore

    /**
     * 获取之前的配置
     * @return StoreConfig
     */
    abstract fun getConfig(): StoreConfig

    /**
     * save value 保存所有类型的方法
     * @param key
     * @param value
     */
    abstract fun save(key: String, value: Any)

    /**
     * remove key 删除XXX key
     * @param key
     */
    abstract fun remove(key: String)

    /**
     * remove key 删除XXX key
     * @param keys
     */
    abstract fun removeKeys(keys: List<String>)

    /**
     * 获取所有的数据(无论是mmkv还是sp 或者是其他)
     */
    abstract fun getAll(): MutableMap<String, *>

    /**
     * 是否包含key
     */
    abstract fun isContainKey(key: String): Boolean

    /**
     *  清除所有数据
     */
    abstract fun clearAll()

    /**
     * 迁移的方法
     * @param values key和value的map
     */
    abstract fun migration(values: MutableMap<String, *>)

    /**
     * 获取字符串
     * @param key key
     * @param default 默认值
     * @return String
     */
    abstract fun getString(key: String, default: String = ""): String

    /**
     * 获取Long
     * @param key key
     * @param default 默认值
     * @return Long
     */
    abstract fun getLong(key: String, default: Long = 0L): Long

    /**
     * 获取Boolean
     * @param key key
     * @param default 默认值
     * @return Boolean
     */
    abstract fun getBoolean(key: String, default: Boolean = false): Boolean

    /**
     * 获取Float
     * @param key key
     * @param default 默认值
     * @return Float
     */
    abstract fun getFloat(key: String, default: Float = 0f): Float

    /**
     * 获取Int
     * @param key key
     * @param default 默认值
     * @return Int
     */
    abstract fun getInt(key: String, default: Int = 0): Int

    /**
     * 获取Double
     * @param key key
     * @param default 默认值
     * @return Double
     */
    abstract fun getDouble(key: String, default: Double = 0.0): Double
}
