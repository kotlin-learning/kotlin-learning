package com.timi.kotlincore.store

import android.content.Context

/**
 * @ClassName: SaveKVImp
 * @Description: 保存key value的实现类
 * @author: lifeng xing
 * @date: 2022/8/2
 */
object StoreImp : IStore {
    private lateinit var mStore: AStore
    override fun init(context: Context): AStore {
        return init(context, StoreConfig())
    }

    override fun init(context: Context, config: StoreConfig): AStore {
        /**
         * 是否初始化
         */
        if (StoreImp::mStore.isInitialized) {
            mStore.init(context)
        } else {
            /**
             * 工厂类根据mSaveKVType初始化
             */
            mStore = StoreFactory.getISaveKVImp(context, config)
            mStore.init(context)
        }
        return mStore
    }

    override fun getConfig(): StoreConfig {
        return mStore.getConfig()
    }

    /**
     * 切换
     */
    override fun swichStore(context: Context, storeType: StoreType): AStore {
        val config = mStore.getConfig()
        config.type = storeType
        mStore = StoreFactory.getISaveKVImp(context, config)
        return mStore
    }

    override fun save(key: String, value: Any) : AStore{
        if (StoreImp::mStore.isInitialized) {
            mStore.save(key, value)
        }
        return mStore
    }

    override fun remove(key: String) : AStore{
        if (StoreImp::mStore.isInitialized) {
            mStore.remove(key)
        }
        return mStore
    }

    override fun removeKeys(keys: List<String>) : AStore{
        if (StoreImp::mStore.isInitialized) {
            mStore.removeKeys(keys)
        }
        return mStore
    }

    override fun getAll(): MutableMap<String, *> {
        if (StoreImp::mStore.isInitialized) {
            return mStore.getAll()
        }
        // 未初始化 直接返回一个空的MutableMap
        return mutableMapOf<String, Any>()
    }

    override fun isContainKey(key: String): Boolean {
        if (StoreImp::mStore.isInitialized) {
            return mStore.isContainKey(key)
        }
        return false
    }

    override fun clearAll() {
        if (StoreImp::mStore.isInitialized) {
            return mStore.clearAll()
        }
    }

    override fun migration(values: MutableMap<String, *>) {
        if (StoreImp::mStore.isInitialized) {
            return mStore.migration(values)
        }
    }

    override fun getString(key: String, default: String): String {
        if (StoreImp::mStore.isInitialized) {
            return mStore.getString(key)
        }
        return default
    }

    override fun getLong(key: String, default: Long): Long {
        if (StoreImp::mStore.isInitialized) {
            return mStore.getLong(key)
        }
        return default
    }

    override fun getBoolean(key: String, default: Boolean): Boolean {
        if (StoreImp::mStore.isInitialized) {
            return mStore.getBoolean(key)
        }
        return default
    }

    override fun getFloat(key: String, default: Float): Float {
        if (StoreImp::mStore.isInitialized) {
            return mStore.getFloat(key)
        }
        return default
    }

    override fun getInt(key: String, default: Int): Int {
        if (StoreImp::mStore.isInitialized) {
            return mStore.getInt(key)
        }
        return default
    }

    override fun getDouble(key: String, default: Double): Double {
        if (StoreImp::mStore.isInitialized) {
            return mStore.getDouble(key)
        }
        return default
    }
}
