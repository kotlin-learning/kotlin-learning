package com.timi.kotlincore.store

import android.content.Context

/**
 * @ClassName: 存储的工厂类
 * @Description: * @author: lifeng xing
 * @date: 2022/8/4
 */
object StoreFactory : ISaveKVFactory {
    /**
     * getISaveKVImp 获取实例
     * @param contextStore Context
     * @param storeConfig StoreConfig
     * @return AStore
     */
    override fun getISaveKVImp(contextStore: Context, storeConfig: StoreConfig): AStore {
        when (storeConfig.type) {
            StoreType.SharePre -> {
                val sharePre = SharePreStore(storeConfig)
                sharePre.init(contextStore, storeConfig)
                return sharePre
            }

            StoreType.MMKV -> {
                val mmkvStore = MMKVStore(storeConfig)
                mmkvStore.init(contextStore, storeConfig)
                return mmkvStore
            }

            StoreType.DataStore -> {
            }
        }
        return MMKVStore(storeConfig)
    }
}

/**
 * 存储配置
 * @param sharePreName      SharePreference name 默认值：config
 * @param rootDir           mmkv 存储路径
 * @param logLevel          日志级别
 * @param defaultBoolean    默认值
 * @param defaultFloat      默认值
 * @param defaultLong       默认值
 * @param defaultInt        默认值
 * @param defaultString     默认值
 * @param defaultDouble     默认值
 * @param type              当前存储的类型：StoreType
 */
data class StoreConfig(
    var sharePreName: String = "config",
    var rootDir: String = "",
    var logLevel: StoreLogLevel = StoreLogLevel.LevelInfo,
    var defaultBoolean: Boolean = false,
    var defaultFloat: Float = 0f,
    var defaultLong: Long = 0L,
    var defaultInt: Int = 0,
    var defaultString: String = "",
    var defaultDouble: Double = 0.0,
    var type: StoreType = StoreType.MMKV,
)
