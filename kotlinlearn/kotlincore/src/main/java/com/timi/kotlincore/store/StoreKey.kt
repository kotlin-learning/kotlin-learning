package com.timi.kotlincore.store

/**
 * @ClassName: 存储的key
 * @Description:
 * @author: lifeng xing
 * @date: 2022/8/4
 */
object StoreKey {
    /**
     * app config key
     */
    val APP_KEY = "app_"
    fun getAppKey(key: String): String = APP_KEY + key

    /**
     *  other key
     */
    val OTHER_KEY = "other_"
    fun getOhterKey(key: String): String = OTHER_KEY + key
}
