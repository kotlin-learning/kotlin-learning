package com.timi.permissionlib

/**
 * 权限工厂的实现类
 *
 * @author lifeng.xing
 *
 * create on 2022/3/3
 */
public class PermissionFactoryImp : PermissionFactory() {
    companion object {
        private lateinit var imp: PermissionFactoryImp
        fun getInstance(): PermissionFactoryImp {
            if (!::imp.isInitialized) {
                imp = PermissionFactoryImp()
            }
            return imp
        }
    }

    override fun <T : PermissionReq?> createPermissionReq(cli: Class<T>?): T? {
        try {
            // 反射
            return cli?.newInstance();
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return null;
    }
}