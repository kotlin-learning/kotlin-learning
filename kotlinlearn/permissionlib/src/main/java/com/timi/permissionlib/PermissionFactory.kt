package com.timi.permissionlib

public abstract class PermissionFactory {
    //创建权限申请
    abstract fun <T : PermissionReq?> createPermissionReq(cli: Class<T>?): T?
}