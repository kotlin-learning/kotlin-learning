package com.timi.permissionlib

import com.vmadalin.easypermissions.EasyPermissions

/**
 * 用于处理权限申请的回调
 *
 * @author lifeng.xing
 *
 * create on 2022/3/3
 */
interface EasyPermissionReqCallback :EasyPermissions.PermissionCallbacks,PermissionCallback{

}