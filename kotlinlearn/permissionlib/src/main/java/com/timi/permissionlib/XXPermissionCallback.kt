package com.timi.permissionlib
/**
 * 用于XXpermission 的回调
 *
 * @author lifeng.xing
 *
 * create on 2022/3/4
 */
interface XXPermissionCallback:PermissionCallback {

    fun onXXPermissionsGranted(all: Boolean, perms: List<String>)

    fun onXXPermissionsDenied(never: Boolean, perms: List<String>)
}