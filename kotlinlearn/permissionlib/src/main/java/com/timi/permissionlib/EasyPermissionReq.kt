package com.timi.permissionlib

import android.app.Activity
import android.text.TextUtils
import android.util.Log
import androidx.annotation.StyleRes
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.annotations.AfterPermissionGranted
import com.vmadalin.easypermissions.models.PermissionRequest
import org.jetbrains.annotations.NotNull
import java.lang.ref.WeakReference

/**
 * 使用EasyPermission
 * 1、使用EasPermission 需要Acitivity实现EasyPermissionReqCallback接口
 * 2、Activity/Fragment 的onRequestPermissionsResult
 *   回调EasyPermissionReq的onRequestPermissionsResult方法传入参数
 * 优点：
 * 1、可定义提示的标题，确认，取消按钮的文字和整个Dialog的样式
 * 2、调用简单，直接将onRequestPermissionsResult的结果传入框架的方法即可
 * 缺点：
 * 1、当申请多个权限的时 如果拒绝其中某个权限直接回调 权限拒绝的方法
 * 2、权限申请提示用户的 弹出框点击取消没有方法回调
 * 3、更改权限申请提示需要反射或直接更改源码才能更改弹出窗的样式
 * 4、@AfterPermissionGranted用于简化代码，当权限全部授于后框架自动调用AfterPermissionGranted注解的方法
 *    Tip：AfterPermissionGranted注解的方法不允许有参数
 * @author lifeng.xing
 *
 * create on 2022/3/3
 */
class EasyPermissionReq : PermissionReq {
    //TAG
    val TAG = "EasyPermissionReq"

    //权限请求的实体
    lateinit var request: PermissionRequest

    //弱引用 activity
    lateinit var actWeak: WeakReference<Activity>

    //默认的提示标题
    val RATIONALE_STR = "应用需要这些权限才能正常运行，请确定授予权限！"

    /**
     * 权限申请
     * @param act Activity
     * @param perms Array<out String>
     */
    override fun requestPermission(act: Activity, vararg perms: String):EasyPermissionReq {
        actWeak = WeakReference(act)
        configPermissionRequest(act, RATIONALE_STR, *perms)
        //请求权限
        requestEasyPermission()
        return this
    }

    /**
     * 配置PermissionRequest
     * @param act Activity
     * @param theme Int
     * @param rationale String
     * @param negative String
     * @param positive String
     */
     fun configPermissionRequest(
        act: Activity,
        @StyleRes theme: Int,
        @NotNull rationale: String,
        @NotNull negative: String,
        @NotNull positive: String
    ) {
        var builder: PermissionRequest.Builder = PermissionRequest.Builder(act)
        builder.rationale(rationale)
        //取消按钮
        if (!TextUtils.isEmpty(negative)) {
            builder.negativeButtonText(negative)
        }
        //确定按钮
        if (!TextUtils.isEmpty(positive)) {
            builder.positiveButtonText(positive)
        }
        //传入request code
        builder.code(1010)
        builder.theme(theme)
        request = builder.build()
    }

    /**
     * 配置PermissionRequest
     * @param act Activity
     * @param rationale String
     * @param perms Array<out String> 权限
     */
    fun configPermissionRequest(act: Activity, rationale: String, vararg perms: String):EasyPermissionReq{
        configPermissionRequest(act, 0, rationale, "", "")
        //已初始化 设置权限
        if (::request.isInitialized) {
            request.perms = perms
        }
        return this
    }

    /**
     * 配置PermissionRequest
     * @param act Activity
     * @param request PermissionRequest
     */
    fun configPermissionRequest(act: Activity, request: PermissionRequest) {
        this.request = request
    }

    /**
     * 定义的EasyPermission request方法
     * @param act Activity
     * @param perms Array<out String>
     */
    @AfterPermissionGranted(1010)
    fun requestEasyPermission() {
        if (EasyPermissions.hasPermissions(actWeak.get(), *request.perms)) {
            Log.i(TAG, "hasPermissions  perms=${request.perms}")
        } else {
            Log.i(TAG, "no hasPermissions")
            //不为空则申请权限
            actWeak.get()?.let {
                EasyPermissions.requestPermissions(it, request)
            }
        }
    }
    /**
     * 需要在Activity中进行回调 在Acitvity 的onRequestPermissionsResult方法传回到EasyPermission中
     * @param requestCode Int
     * @param permissions Array<String>
     * @param grantResults IntArray
     */
     fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        actWeak.get()?.let {
            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults,
                it
            )
        }
    }

}