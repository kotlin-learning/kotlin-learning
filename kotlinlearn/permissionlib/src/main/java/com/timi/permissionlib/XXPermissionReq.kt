package com.timi.permissionlib

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.XXPermissions

/**
 * 使用XXPermissionReq
 *
 * @author lifeng.xing
 *
 * create on 2022/3/3
 */
class XXPermissionReq : PermissionReq {
    val TAG = "XXPermissionReq"

    /**
     * 权限申请
     * @param act Activity
     * @param perms Array<out String>
     */
    override fun requestPermission(act: Activity, vararg perms: String): XXPermissionReq {
        XXPermissions.with(act).permission(perms).request(object : OnPermissionCallback {
            override fun onGranted(permissions: MutableList<String>?, all: Boolean) {
                Log.i(TAG, "onGranted permissions=$permissions")
                //是否获取了所有的权限
                if (act is XXPermissionCallback) {
                    permissions?.let { act.onXXPermissionsGranted(all, it) }
                }
            }

            override fun onDenied(permissions: MutableList<String>?, never: Boolean) {
                super.onDenied(permissions, never)
                Log.i(TAG, "onDenied permissions=$permissions")
                //是否永久拒绝权限
                if (never) {
                    //被永久拒绝权限，去往设置进行更改
                    XXPermissions.startPermissionActivity(act, permissions);
                } else {
                    if (act is XXPermissionCallback) {
                        permissions?.let { act.onXXPermissionsDenied(never, it) }
                    }
                }
            }
        })
        return this
    }

    /**
     * 判断一个/多个权限是否被授予
     * @param context Context
     * @param perms Array<out String>
     * @return Boolean
     */
    fun isGranted(context: Context, vararg perms: String) = XXPermissions.isGranted(context, perms)

    /**
     * 获取没有授予的权限
     * @param context Context
     * @param perms Array<out String>
     * @return (MutableList<(String..String?)>..List<(String..String?)>?)
     */
    fun getDenied(context: Context, vararg perms: String) = XXPermissions.getDenied(context, perms)

    /**
     * 判断某个权限是否为特殊权限
     * @param perms String
     * @return Boolean
     */
    fun isSpecialPermission(perms: String) = XXPermissions.isSpecial(perms)

    /**
     * 判断一个/多个权限被永久拒绝
     * @param act Activity
     * @param perms Array<out String>
     * @return Boolean
     */
    fun isPermanentDenied(act: Activity, vararg perms: String) =
        XXPermissions.isPermanentDenied(act, perms)

    /**
     * 跳转到应用设置的详情页
     * @param context Context
     * @param perms Array<out String>
     */
    fun startPermissionActivity(context: Context, vararg perms: String) {
        if (perms.isEmpty()) {
            XXPermissions.startPermissionActivity(context)
        } else
            XXPermissions.startPermissionActivity(context, perms)
    }
    /**
     * 跳转到应用设置的详情页
     * @param act Activity
     * @param perms Array<out String>
     */
    fun startPermissionActivity(act: Activity, vararg perms: String) {
        if (perms.isEmpty()) {
            XXPermissions.startPermissionActivity(act)
        } else
            XXPermissions.startPermissionActivity(act, perms)
    }
    /**
     * 跳转到应用设置的详情页
     * @param fragment: Fragment
     * @param perms Array<out String>
     */
    fun startPermissionActivity(fragment: Fragment, vararg perms: String) {
        if (perms.isEmpty()) {
            XXPermissions.startPermissionActivity(fragment)
        } else
            XXPermissions.startPermissionActivity(fragment, perms)
    }
}