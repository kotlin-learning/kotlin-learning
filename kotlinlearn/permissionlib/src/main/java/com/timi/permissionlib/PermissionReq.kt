package com.timi.permissionlib

import android.app.Activity
import androidx.annotation.Size
/**
 * 工厂模式 定义的权限申请的接口
 *
 * @author lifeng.xing
 *
 * create on 2022/3/3
 */
public interface PermissionReq {
    /**
     * 请求权限定义的接口
     * @param act Activity
     * @param perms Array<out String>
     */
    fun requestPermission(act: Activity, @Size(min = 1) vararg perms: String):PermissionReq

}