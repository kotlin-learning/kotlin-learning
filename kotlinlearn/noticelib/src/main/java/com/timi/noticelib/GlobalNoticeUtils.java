package com.timi.noticelib;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

/**
 * @ClassName: GlobalNoticeUtils
 * @Description: 用于全局通知的工具类
 * 1、view 外部传入
 * 2、动画可配置
 * 3、内容可配置
 * @author: lifeng xing
 * @date: 2022/6/11
 */
public class GlobalNoticeUtils {
    /**
     * ApplicationContext
     */
    private Context ctx;
    /**
     * 用于处理上划事件
     */
    private float mPosX, mPosY, mCurPosX, mCurPosY;
    /**
     * 单例
     */
    public static GlobalNoticeUtils instance = null;

    public GlobalNoticeUtils(Context ctx) {
        this.ctx = ctx.getApplicationContext();
    }

    public static GlobalNoticeUtils getInstance(Context ctx) {
        if (instance == null) {
            synchronized (GlobalNoticeUtils.class) {
                if (instance == null) {
                    instance = new GlobalNoticeUtils(ctx);
                }
            }
        }
        return instance;
    }

    private View mRootView = null;
    /**
     * 消息内容
     */
    private String mContent = null;
    /**
     * 时间
     */
    private String mTime = null;
    /**
     * 图片地址默认为空使用app的图标
     */
    private String mImgUrl = null;
    /**
     * 图片的资源id
     */
    private int mImgResourseId = -1;
    /**
     * 动画时间
     */
    private int mDefaulAnimaDuration = 3000;
    /**
     * 默认动画Y轴的距离
     */
    private int mDefaultTransY = 100;
    /**
     * 默认高度
     */
    private int mDefaultTransHeight = 60;
    /**
     * 状态栏高度
     */
    private int mStatusBarHeight = 20;
    /**
     * 点击事件
     */
    private View.OnClickListener listener;
    /**
     * 消息 图标加载的回调
     */
    private NoticeImageLoadListener imageLoadListener;
    /**
     * 动画
     */
    private Animator mAnimator = null;

    /**
     * 设置默认显示的内容
     *
     * @param mContent
     * @return
     */
    public GlobalNoticeUtils setDefaultContent(@NonNull String mContent) {
        this.mContent = mContent;
        return this;
    }

    /**
     * 设置默认显示的时间
     *
     * @param mTime
     * @return
     */
    public GlobalNoticeUtils setDefaultTime(@NonNull String mTime) {
        this.mTime = mTime;
        return this;
    }

    /**
     * 设置默认的图标
     *
     * @param mImgUrl
     * @return
     */
    public GlobalNoticeUtils setDefaultImageUrl(@NonNull String mImgUrl) {
        this.mImgUrl = mImgUrl;
        return this;
    }

    /**
     * 设置默认的图标
     *
     * @param imgResourseId
     * @return
     */
    public GlobalNoticeUtils setDefaultmImgResourseId(@DrawableRes int imgResourseId) {
        this.mImgResourseId = imgResourseId;
        return this;
    }

    /**
     * 设置默认动画的时间
     *
     * @param duration
     * @return
     */
    public GlobalNoticeUtils setDefaultAnimaDuration(int duration) {
        this.mDefaulAnimaDuration = duration;
        return this;
    }

    /**
     * 设置状态栏的高度
     *
     * @param heght
     * @return
     */
    public GlobalNoticeUtils setStatusBarHeight(int heght) {
        this.mStatusBarHeight = heght;
        return this;
    }

    /**
     * 设置默认动画的从上而下和从下至上的距离
     *
     * @param value
     * @return
     */
    public GlobalNoticeUtils setDefaultAnimaTransY(int value) {
        this.mDefaultTransY = value;
        return this;
    }

    /**
     * 设置默认view高度
     *
     * @param value
     * @return
     */
    public GlobalNoticeUtils setDefaultHeight(int value) {
        this.mDefaultTransHeight = value;
        return this;
    }

    /**
     * 设置动画
     *
     * @param mAnimator
     * @return
     */
    public GlobalNoticeUtils setAnimator(Animator mAnimator) {
        this.mAnimator = mAnimator;
        return this;
    }

    /**
     * 显示APP内部消息
     *
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("ClickableViewAccessibility")
    public GlobalNoticeUtils startShow() {
        //初始化view
        if (null == mRootView) {
            mRootView = defaultNotcieView();
        }
        //设置内容 时间 等
        if (null != mRootView) {
            //设置默认NoticeView的其他属性如 消息内容 消息时间等
            ImageView ivIcon = mRootView.findViewById(R.id.iv_notice);
            if (!TextUtils.isEmpty(mImgUrl) && null != ivIcon) {
                //地址
                if (null != imageLoadListener) {
                    imageLoadListener.loadImgUrl(ivIcon);
                }
                //资源id
                if (mImgResourseId != -1) {
                    try {
                        ivIcon.setImageResource(mImgResourseId);
                    } catch (Exception e) {
                        //catch 一下防止传入的参数不正确
                    }
                }
            }
            //内容
            TextView tvContent = mRootView.findViewById(R.id.tv_notice_msg);
            if (!TextUtils.isEmpty(mContent) && null != tvContent) {
                tvContent.setText(mContent);
            }
            //时间 默认显示现在
            TextView tvTimeStr = mRootView.findViewById(R.id.tv_notice_time);
            if (!TextUtils.isEmpty(mTime) && null != tvTimeStr) {
                tvTimeStr.setText(mTime);
            }
        }
        //消息点击的时间
        if (null != mRootView) {
            mRootView.setOnClickListener(v -> {
                if (null != listener) {
                    listener.onClick(v);
                }
            });
        }
        //设置大小 防止点击事件影响原来界面上的点击事件
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp2px(mDefaultTransHeight));
        mRootView.setLayoutParams(layoutParams);
        //添加动画
        if (null == mAnimator) {
            mAnimator = defaultAnimator(mRootView);
        }
        mRootView.setOnTouchListener((@SuppressLint("ClickableViewAccessibility") View v, @SuppressLint("ClickableViewAccessibility") MotionEvent event) -> {

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    mPosX = event.getX();
                    mPosY = event.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    mCurPosX = event.getX();
                    mCurPosY = event.getY();

                    break;
                case MotionEvent.ACTION_UP:
                    float upY = mCurPosY - mPosY;
                    float upX = mCurPosX - mPosX;
                    if (Math.abs(upY) > Math.abs(upX)) {
                        if (upY > 0) {
                            listener.onClick(v);
                        } else {
                            //上划隐藏
                            hideGlobalNotice(mRootView);
                        }
                    } else {
                        if (upX > 0) {
                            listener.onClick(v);
                        } else {
                            listener.onClick(v);
                        }
                    }
                    break;
                default:
                    break;
            }
            return true;
        });
        //显示添加的消息view
        if(null!=mNoticeCallback){
            mNoticeCallback.showNotice(mRootView);
        }
        //震动
        Vibrator vibrator = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE));
        //动画启动
        mAnimator.start();
        isHide = true;
        return this;
    }

    /**
     * 默认的消息布局 View
     *
     * @return
     */
    private View defaultNotcieView() {
        @SuppressLint("InflateParams") View inflate = LayoutInflater.from(ctx).inflate(R.layout.view_global_notice, null);
        return inflate;
    }

    /**
     * 默认动画效果，从顶部弹出，回弹，时间结束隐藏View
     *
     * @param mRootView
     * @return
     */
    private Animator defaultAnimator(View mRootView) {
        //创建属性动画,从下到上
        ObjectAnimator bottomToTop = ObjectAnimator.ofFloat(mRootView, "translationY", mStatusBarHeight, -dp2px(mDefaultTransY)).setDuration(300);
        //创建属性动画,从上到下
        ObjectAnimator topToBottom = ObjectAnimator.ofFloat(mRootView, "translationY", -dp2px(mDefaultTransY), mStatusBarHeight).setDuration(500);
        //初始化动画组合器
        AnimatorSet animator = new AnimatorSet();
        //动画时间
        animator.play(bottomToTop).after(topToBottom).after(mDefaulAnimaDuration);
        //添加动画结束回调
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                //删除View
                hideGlobalNotice(mRootView);
            }
        });
        return animator;
    }

    /**
     * 增加监听器
     *
     * @param listener
     * @return
     */
    public GlobalNoticeUtils setNoticeOnclickListener(View.OnClickListener listener) {
        this.listener = listener;
        return this;
    }

    /**
     * 当前是否隐藏
     *
     * @return
     */
    public boolean isHide() {
        return isHide;
    }

    private boolean isHide = false;

    /**
     * 隐藏View
     *
     * @param view
     * @return
     */
    public GlobalNoticeUtils hideGlobalNotice(View view) {
        if(null!=mNoticeCallback){
            mNoticeCallback.hideNotice(view);
        }
        isHide = true;
        return instance;
    }

    private NoticeCallback mNoticeCallback = null;

    public GlobalNoticeUtils setCallback(NoticeCallback callback) {
        this.mNoticeCallback = callback;
        return this;
    }

    /**
     * 显示View 到当前显示的Activity的DecorView
     *
     * @param curActivity
     * @param view
     */
    public void showNoticeToDecorView(Activity curActivity, View view) {
        /*Activity不为空并且没有被释放掉*/
        if (curActivity != null && !curActivity.isFinishing()) {
            /*获取Activity顶层视图,并添加自定义View*/
            try {
                ViewGroup group = ((ViewGroup) curActivity.getWindow().getDecorView());
                //移除
                ViewGroup groupParent = (ViewGroup) view.getParent();
                if (null != groupParent) {
                    groupParent.removeView(view);
                }
                group.addView(view);
            } catch (Exception ignored) {
                Log.i("showView", "Exception ignored=" + ignored.getMessage());
            }
        }
    }

    /**
     * 隐藏View 到当前显示的Activity的DecorView
     *
     * @param curActivity
     * @param view
     */
    public void hideNoticeToDecorView(Activity curActivity, View view) {
        /*Activity不为空并且没有被释放掉*/
        if (curActivity != null && !curActivity.isFinishing()) {
            /*获取Activity顶层视图*/
            ViewGroup root = ((ViewGroup) curActivity.getWindow().getDecorView());
            /*如果Activity中存在View对象则删除*/
            if (root.indexOfChild(view) != -1) {
                /*从顶层视图中删除*/
                root.removeView(view);
            }
        }
    }

    /**
     * 从dp单位转换为px
     *
     * @param dp dp值
     * @return 返回转换后的px值
     */
    private int dp2px(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    /**
     * 图片加载的监听器
     */
    interface NoticeImageLoadListener {
        /**
         * 加载图片
         *
         * @param imageView
         */
        void loadImgUrl(ImageView imageView);
    }

    /**
     * 显示和隐藏 view
     */
    interface NoticeCallback {
        void showNotice(View view);

        void hideNotice(View view);
    }
}
