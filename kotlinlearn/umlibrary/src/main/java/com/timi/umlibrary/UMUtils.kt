package com.timi.ummodule

import android.content.Context
import com.timi.umlibrary.constant.Constants
import com.umeng.analytics.MobclickAgent
import com.umeng.commonsdk.UMConfigure

/**
 * @ClassName: UMUtils
 * @Description: UM utils
 * @author: lifeng xing
 * @date: 2022/6/6
 */
object UMUtils {
    /**
     * 初始化 可以在线程中初始化
     * 确保App首次冷启动时，在用户阅读您的《隐私政策》并取得用户授权之后，
     * 才调用正式初始化函数UMConfigure.init()初始化统计SDK，
     * 此时SDK才会真正采集设备信息并上报数据。反之，如果用户不同意《隐私政策》授权，
     * 则不能调用UMConfigure.init()初始化函数。
     */
    fun init(ctx: Context): UMUtils {
        UMConfigure.init(
            ctx,
            Constants.UMAppKey,
            Constants.UMChannel,
            UMConfigure.DEVICE_TYPE_PHONE,
            Constants.UMPushSecret,
        )
        return this
    }

    fun init(ctx: Context, appKey: String, channel: String, pushSecret: String): UMUtils {
        UMConfigure.init(
            ctx,
            Constants.UMAppKey,
            Constants.UMChannel,
            UMConfigure.DEVICE_TYPE_PHONE,
            pushSecret,
        )
        return this
    }

    /**
     * 自动采集
     * @return UMUtils
     */
    fun autoCollect(): UMUtils {
        // 选用AUTO页面采集模式
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO)
        return this
    }

    /**
     * 初始化前的准备
     * 预初始化函数不会采集设备信息，也不会向友盟后台上报数据。
     */
    fun preInit(ctx: Context): UMUtils {
        preInit(ctx, Constants.UMAppKey, Constants.UMChannel)
        return this
    }

    /**
     * 初始化前的准备
     * 预初始化函数不会采集设备信息，也不会向友盟后台上报数据。
     * @param ctx
     * @param appKey  um 申请的app key
     * @param channel 渠道
     */
    fun preInit(ctx: Context, appKey: String, channel: String): UMUtils {
        UMConfigure.preInit(ctx, appKey, channel)
        return this
    }

    /**
     * 友盟 自身账户登录
     */
    fun signIn(userId: String): UMUtils {
        MobclickAgent.onProfileSignIn(userId)
        return this
    }

    /**
     * 友盟 第三方账户登录
     */
    fun signIn(thirdTip: String, userId: String): UMUtils {
        MobclickAgent.onProfileSignIn(thirdTip, userId)
        return this
    }

    /**
     * 友盟 第三方账户登录
     */
    fun signOff(): UMUtils {
        // 登出
        MobclickAgent.onProfileSignOff()
        return this
    }
}
