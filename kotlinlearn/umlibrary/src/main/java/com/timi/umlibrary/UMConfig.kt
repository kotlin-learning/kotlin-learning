package com.timi.umlibrary

import com.timi.umlibrary.constant.Constants

/**
 * @ClassName: UMConfig
 * @Description:  友盟的Config类
 * @author: lifeng xing
 * @date: 2022/6/6
 */
class UMConfig(private var appKey: String) {
    /**
     * 友盟的APPKey
     */
    /**
     * 友盟的渠道 通过打包的渠道来获取
     */
    private var pushSecret: String = Constants.UMPushSecret

    /**
     * 友盟推送的secret
     */
    private var channel: String = Constants.UMChannel

    /**
     * 友盟Log开关 可直接通过BuildConfig.Debug设置
     */
    private var isLog: Boolean = false

    /**
     * 设置友盟appkey
     * @param appKey
     */
    fun setAppKey(appKey: String): UMConfig {
        this.appKey = appKey
        return this
    }

    /**
     * 设置友盟推送的pushSecret
     * @param pushSecret
     */
    fun setPushSecret(pushSecret: String): UMConfig {
        this.pushSecret = pushSecret
        return this
    }

    /**
     * 设置友盟渠道
     * @param channel
     */
    fun setChannel(channel: String): UMConfig {
        this.channel = appKey
        return this
    }

    /**
     * 设置友盟调试开关
     * @param isLog
     */
    fun setChannel(isLog: Boolean): UMConfig {
        this.isLog = isLog
        return this
    }
}
