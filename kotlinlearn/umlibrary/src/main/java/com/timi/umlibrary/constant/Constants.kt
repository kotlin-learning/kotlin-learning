package com.timi.umlibrary.constant

/**
 * @ClassName: Constants
 * @Description: 一些静态变量
 * @author: lifeng xing
 * @date: 2022/6/6
 */
object Constants {
    /**
     * 友盟的APPKey
     */
    const val UMAppKey = "629d716805844627b5a1480e"

    /**
     * 友盟的渠道
     */
    const val UMChannel = ""

    /**
     * 友盟推送的secret
     */
    const val UMPushSecret = "a52a25ec513575bb9a0beedbbaf51f76"

    /**
     * 隐私政策地址
     */
    const val UMPivacyUrl = "https://www.umeng.com/page/policy"
}
