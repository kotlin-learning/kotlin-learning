package com.timi.kotlinlearn

import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

/**
 * @ClassName: TestScope
 * @Description: 测试Scope
 * @author: lifeng xing
 * @date: 2023/4/19
 */
class TestScope

fun main() {
    testAsyncError()
    testAsync()
    testTryCatch()
}

/**
 * 测试Async错误的使用方法
 * CoroutineStart.LAZY 不会主动启动协程，
 * 而是直到调用async.await()或者async.satrt()后才会启动（即懒加载模式）
 */
fun testAsyncError() {
    val measureTimeMillis = measureTimeMillis {
        runBlocking {
            val asyncA = async(start = CoroutineStart.LAZY) {
                delay(3000)
                1
            }
            asyncA.start()
            val asyncB = async(start = CoroutineStart.LAZY) {
                delay(4000)
                2
            }
            asyncB.start()
            println("asyncA" + "=" + asyncA.await())
            println("asyncB" + "=" + asyncB.await())
        }
    }
    println("measureTimeMillis=$measureTimeMillis")
}

/**
 * 测试Async
 */
fun testAsync() {
    val measureTimeMillis4 = measureTimeMillis {
        runBlocking {
            val asyncA = async(start = CoroutineStart.DEFAULT) {
                delay(3000)
                1
            }
            asyncA.start()
            val asyncB = async(start = CoroutineStart.DEFAULT) {
                delay(4000)
                2
            }
            asyncB.start()
            println("asyncA" + "=" + asyncA.await())
            println("asyncB" + "=" + asyncB.await())
        }
    }
    println("measureTimeMillis=$measureTimeMillis4")
}

/**
 * 测试try catch
 */
fun testTryCatch() {
    // try catch
    measureTimeMillis {
        runBlocking {
            val job = launch {
                try {
                    repeat(1000) { i ->
                        println("testTryCatch: I'm sleeping $i ...")
                        delay(500L)
                    }
                } catch (e: CancellationException) {
                    println("testTryCatch:CancellationException ${e.message}")
                } finally {
                    println("testTryCatch: I'm running finally")
                }
            }
            delay(1300L)
            println("testTryCatch: I'm tired of waiting!")
            job.cancelAndJoin()
            println("testTryCatch: Now I can quit.")
        }
    }
}
