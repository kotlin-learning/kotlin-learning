package com.timi.kotlinlearn

import org.junit.Test

class ZipTest {
    @Test
    fun test_Zip() {
        val fruits = listOf("apple", "banana", "pair")
        val colors = listOf("red", "yellow")
        print(colors.zip(fruits) { color, animal -> "the ${animal.replaceFirstChar{it.lowercase()}} is $color" })
    }
}
