package com.timi.kotlinlearn.scope

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.yield

fun main() {
    testSuspend()
}

/**
 * 测试suspend
 */
fun testSuspend() {
    runBlocking {
        val scope = launch(Dispatchers.IO) {
            readFile()
        }
        Thread.sleep(1000)
        scope.cancel()
        println("模拟手动取消")
    }
}

suspend fun readFile() {
    println("模拟读取文件开始")
    Thread.sleep(2000)
    yield()
    println("模拟读取文件结束")
}
