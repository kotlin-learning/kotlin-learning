package com.timi.kotlinlearn.event

data class AppEvent(val key: String, val value: String)
