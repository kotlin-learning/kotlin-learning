package com.timi.kotlinlearn.kotlin

/**
 * @Author: create by timi$
 * @time : create by 2021年11月22日19:08:00$
 * @function: 静态方法
 *  object修饰的类 可以使用类似于静态方法的形式调用其内部的方法但是它的方法并不是静态方法
 */
class StaticDemo {
    fun demo() {
        // 使用object修饰的单例 调用方式和调用静态方法一致
        ObjectDemo.doStaticMethod()
        // 调用静态方法一样的方式调用
        CompanionObject.doCompanionMethod()
        // 只能使用实例化之后调用
        CompanionObject().normalMethod()

        // 真正的静态方法
        CompanionObject.doCompanionStaticMethod()
        // 顶层方法直接调用
        topMethodDemo()
    }
}

/**
 * 以object修饰的单例
 * 调用方式和调用静态方法一致
 */
object ObjectDemo {
    fun doStaticMethod() {
        println("doStaticMethod")
    }
}

/**
 * 普通的类 使用 companion object
 * 可以将一部分方法以
 * CompanionObject.doCompanionMethod()的方式调用
 *
 * companion object的实质是在其内部创建一个伴生类，
 * 而doCompanionMethod定义在这个伴生类中
 * 因此调用CompanionObject.doCompanionMethod()方法
 * 其实是调用CompanionObject类中伴生对象的doCompanionMethod方法
 *
 * 真正的静态方法 1: @JvmStatic 以注解的形式必须是companion object内的方法
 *                             如果是普通方法会报错
 *              2: 顶层方法    如TopMethod.
 */
class CompanionObject {
    companion object {
        fun doCompanionMethod() {
            println("doCompanionMethod")
        }
        @JvmStatic
        fun doCompanionStaticMethod() {
            println("doCompanionStaticMethod")
        }
    }
    fun normalMethod() {
        println("normalMethod")
    }
}
