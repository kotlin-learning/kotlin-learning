package com.timi.kotlinlearn

import androidx.lifecycle.* // ktlint-disable no-wildcard-imports

/**
 * @Author: create by timi
 * @time : create by 2021年11月24日19:57:15
 * @function:
 *  tip：在ViewModel中使用协程的时候推荐使用
 *  viewModelScope,自动绑定viewmodel的生命周期当viewmodel生命周期结束自动清除掉viewModelScope
 *
 */
class MainViewModel : ViewModel() {
    private var signalLiveData: MutableLiveData<SignalBean> = MutableLiveData<SignalBean>()
    fun updateSignal(level: Int, isNet: Boolean, isWifi: Boolean) {
        val signalBean = SignalBean(level, isNet, isWifi)
        signalLiveData.postValue(signalBean)
    }

    // 获取实例
    fun getSignalLiveData() = signalLiveData
}
