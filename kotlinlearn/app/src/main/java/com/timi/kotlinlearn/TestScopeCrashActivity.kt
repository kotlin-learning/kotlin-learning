package com.timi.kotlinlearn

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.NullPointerException

/**
 * @ClassName:TestScopeCrashActivity
 * @Description: 测试协程异常
 * @author: lifeng xing
 * @date: 2023/6/13
 */
class TestScopeCrashActivity : AppCompatActivity() {
    val TAG = "TestScopeCrashActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_scope_crash)
        testSupervisorJob()
        testAsyncCatchExp()
        testAsyncCatchSupervisorJob()
        testSupervisorJobAndTryCatch()
    }

    /**
     * SupervisorJob+tryCatch用于比如我们某个界面需要获取列表，需要获取用户权限，
     * 但是我们需要列表正常获取，当用户权限接口异常的时候弹窗提示用户权限不足且要取消列表
     */
    private fun testSupervisorJobAndTryCatch() {
        val coroutineScope = CoroutineScope(Job())
        coroutineScope.launch {
            val jobGetList = async(SupervisorJob()) {
                Log.i(TAG, "testSupervisorJobAndTryCatch jobA:延迟1000")
                delay(5000)
                Log.i(TAG, "testSupervisorJobAndTryCatch jobA:异常")
                throw NullPointerException()
            }
            val jobUserPermission = async(SupervisorJob()) {
                Log.i(TAG, "testSupervisorJobAndTryCatch jobB:开始")
                delay(100)
                Log.i(TAG, "testSupervisorJobAndTryCatch jobB:异常")
                throw NullPointerException()
                Log.i(TAG, "testSupervisorJobAndTryCatch jobB:结束")
                200
            }
            // 不能先jobA.await()然后在jobB.await() 这样会导致不能取消jobA
            // async 是并行的但是外面是串行的会先等待jobA的结果
            val resultB = kotlin.runCatching { jobUserPermission.await() }
            Log.i(TAG, "testSupervisorJobAndTryCatch resultB=$resultB")
            if (resultB.isFailure) {
                jobGetList.cancel()
                Log.i(TAG, "testSupervisorJobAndTryCatch jobA:cancel")
            }
            val resultA = kotlin.runCatching { jobGetList.await() }
            Log.i(TAG, "testSupervisorJobAndTryCatch resultA=$resultA")
        }
    }

    /**
     * 捕获Async异常
     */
    private fun testAsyncCatchExp() {
        Log.i(TAG, "testAsyncCatchExp:开始")
        val coroutineScope = CoroutineScope(Job())
        coroutineScope.launch {
            // 如果不加入SupervisorJob表示子协程不自己处理
            // 则会直接crash 并不能通过runCatching捕获到异常
            val async = async(SupervisorJob()) {
                Log.i(TAG, "testAsyncCatchExp async:开始")
                delay(10)
                Log.i(TAG, "testAsyncCatchExp async:结束")
                Log.i(TAG, "testAsyncCatchExp async:抛出异常")
                throw NullPointerException()
            }
            val async1 = async(SupervisorJob()) {
                Log.i(TAG, "testAsyncCatchExp async1:开始")
                delay(10)
                Log.i(TAG, "testAsyncCatchExp async1:结束")
            }
            val result = kotlin.runCatching { async.await() }
            Log.i(TAG, "testAsyncCatchExp async:kotlin.runCatching result=$result")
            val result1 = kotlin.runCatching { async1.await() }
            Log.i(TAG, "testAsyncCatchExp async1:kotlin.runCatching result1=$result1")
        }
    }

    /**
     * 捕获Async子协程 异常
     */
    private fun testAsyncCatchSupervisorJob() {
        Log.i(TAG, "testAsyncCatchSupervisorJob:开始")
        val coroutineScope =
            CoroutineScope(
                SupervisorJob() +
                    CoroutineExceptionHandler { coroutineContext, throwable ->
                        Log.i(TAG, "testAsyncCatchSupervisorJob CoroutineExceptionHandler")
                    },
            )
        coroutineScope.launch {
            // 如果不加入SupervisorJob表示子协程不自己处理
            // 则会直接crash 并不能通过runCatching捕获到异常
            val async = async {
                Log.i(TAG, "testAsyncCatchSupervisorJob async:开始")
                delay(10)
                Log.i(TAG, "testAsyncCatchSupervisorJob async:结束")
                Log.i(TAG, "testAsyncCatchSupervisorJob async:抛出异常")
                throw NullPointerException()
            }
            val result = kotlin.runCatching { async.await() }
            Log.i(TAG, "testAsyncCatchSupervisorJob async:kotlin.runCatching result=$result")
        }
    }

    /**
     * 测试SupervisorJob()
     *
     */
    fun testSupervisorJob() {
        Log.i(TAG, "testSupervisorJob:开始")
        val coroutineScope =
            CoroutineScope(
                SupervisorJob() + CoroutineExceptionHandler { coroutineContext, throwable ->
                    Log.i(TAG, "CoroutineExceptionHandler+${throwable.cause}")
                },
            )
        coroutineScope.launch(CoroutineName("A")) {
            delay(10)
            Log.i(TAG, "testSupervisorJob:抛出异常")
            throw RuntimeException()
        }
        coroutineScope.launch(CoroutineName("B")) {
            delay(100)
            Log.i(TAG, "testSupervisorJob:正常执行，不会受到影响")
        }
        Log.i(TAG, "testSupervisorJob:结束")
    }
}
