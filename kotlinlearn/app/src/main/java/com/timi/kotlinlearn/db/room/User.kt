package com.timi.kotlinlearn.db.room

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @ClassName: Upload
 * @Description: 上传文件的实体类
 * @author: lifeng xing
 * @date: 2023/6/3
 */
@Entity(tableName = "uplpads")
data class Upload(
    @PrimaryKey val id: Int,
    val fileName: String? = "",
    val fileSize: String? = "",
    val fileSnap: String? = "",
    val fileAddress: String? = "",
    val extra: String? = "",
)
