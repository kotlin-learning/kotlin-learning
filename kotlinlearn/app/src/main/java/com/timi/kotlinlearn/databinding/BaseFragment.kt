package com.timi.kotlinlearn.databinding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

/**
 *  用于DataBinding的基类
 *  @author lifeng.xing timi
 *  created at 2021/12/29  14:50
 */
abstract class BaseFragment<VB : ViewDataBinding> : Fragment() {
    //
    protected lateinit var mBinding: VB
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = getViewBinding(inflater)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        // 判断是否初始化，取消绑定，避免内存泄露
        if (::mBinding.isInitialized) {
            mBinding.unbind()
        }
    }
}
