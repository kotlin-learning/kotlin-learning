package com.timi.kotlinlearn.flow

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner

/**
 * 用于ShareFlow发送事件
 */

// Application范围的事件
/**
 * @param event 发送的事件
 * @param timeMillis 延时
 */
inline fun <reified T> postEvent(event: T, timeMillis: Long = 0L) {
    ShareFlowBus.getApplicationScopeViewModel(ShareFlowBusCore::class.java)
        .postEvent(T::class.java.name, event!!, timeMillis)
}
/**
 * @param scope 某一限定范围内的事件 如：Activity内，Fragment等等
 * @param event 发送的事件
 * @param timeMillis 延时
 */
// 限定范围的事件
inline fun <reified T> postEvent(scope: ViewModelStoreOwner, event: T, timeMillis: Long = 0L) {
    ViewModelProvider(scope).get(ShareFlowBusCore::class.java)
        .postEvent(T::class.java.name, event!!, timeMillis)
}
