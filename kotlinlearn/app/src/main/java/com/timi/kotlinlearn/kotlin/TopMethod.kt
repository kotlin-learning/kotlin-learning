package com.timi.kotlinlearn.kotlin

/**
 * @Author: create by timi$
 * @time : create by 2021年11月22日19:20:14$
 * @function: 顶层方法
 *      直接使用topMethodDemo调用
 *      java中使用TopMethodKt.topMethodDemo调用
 *
 */
fun topMethodDemo() {
    println("topMethodDemo")
}
