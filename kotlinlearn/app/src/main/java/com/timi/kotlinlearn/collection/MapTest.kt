package com.timi.kotlinlearn.collection

import android.os.Build
import androidx.annotation.RequiresApi

/**
 * @ClassName: MapTest
 * @Description: map的测试类
 * @author: lifeng xing
 * @date: 2023/5/18
 */
@RequiresApi(Build.VERSION_CODES.N)
fun main() {
    val mapsNum = mapOf(
        Pair(1, "one"),
        Pair(2, "two"),
        Pair(3, "three"),
        Pair(4, "four"),
        Pair(5, "five"),
    )
    println(mapsNum)
    println("mapsNum.get(1)=" + mapsNum.get(1))
    println("mapsNum[1]=" + mapsNum[1])
    // 防止崩溃取值方式：
    // getOrElse()  与 list 的工作方式相同：对于不存在的键，其值由给定的 lambda 表达式返回
    // getOrDefault()  如果找不到键，则返回指定的默认值
    // 支持版本24
    println("mapsNum[10]=" + mapsNum.getOrDefault(10, "none"))
    println("mapsNum[1001]=" + mapsNum.getOrElse(1001) { "getOrElse 1001" })
    // filterKeys
    val filterKeyMaps = mapsNum.filterKeys { it == 1 }
    println("filterKeys=$filterKeyMaps")
    // filterValues
    val filterValueMaps = mapsNum.filterValues { it == "one" }
    println("filterValueMaps=$filterValueMaps")
    // filter
    val filterMaps = mapsNum.filter { (key, value) -> key == 1 && value == "one" }
    println("filterMaps=$filterMaps")

    // 加减操作符
    println("plus=${mapsNum.plus(Pair(6, "six"))}")
    println("plus to=${mapsNum.plus(6 to "six")}")
    println("minus=${mapsNum.minus(Pair(6, "six"))}")

    // mutableMapOf
    val mutableMapsNum = mutableMapOf("one" to 1, "two" to 2, "three" to 3, "four" to 4)
    mutableMapsNum["three"] = 3
    println("mutableMapsNum put =$mutableMapsNum")
    mutableMapsNum.putAll(mutableMapOf("five" to 5))
    println("mutableMapsNum putAll=$mutableMapsNum")
}
