package com.timi.kotlinlearn.kotlin

/**
 * @Author: create by timi
 * @time : create by 2021年11月23日20:28:05
 * @function: infix函数 主要用途让语法更具有可读性
 *
 */
/**
 * 1、infix函数不能定义成顶层函数 必须是某个类的成员函数
 * 2、infix函数必须接收且只能接收一个参数
 */
infix fun String.beginWith(prefix: String) = startsWith(prefix)

/**
 * 判断是否包含某个指定的元素
 */
infix fun <T> Collection<T>.has(element: T) = contains(element)

/**
 * 创建Map的Pair
 * @receiver A
 * @param that B
 * @return Pair<A, B>
 */
infix fun <A, B> A.with(that: B): Pair<A, B> {
    return Pair(this, that)
}

fun main() {
    println("Hello Boy".beginWith("hello"))
    val listOf = listOf("apple", "banana", "pair", "orange")
    println(listOf.has("apple"))
    val map =
        mapOf("Apple" with 1, "Banana" with 2, "Orange" with 3, "Pear" with 4, "Grape" with 5)
    println("map=$map")
}
