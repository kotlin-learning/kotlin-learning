package com.timi.kotlinlearn

import android.app.Application
import androidx.lifecycle.ProcessLifecycleOwner
import com.timi.kotlincore.store.StoreConfig
import com.timi.kotlincore.store.StoreImp
import com.timi.kotlincore.store.StoreType
import com.timi.kotlinlearn.flow.ShareFlowBus
import com.timi.kotlinlearn.lifecycle.ApplicationObserver
import com.timi.ummodule.UMUtils

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // 增加Application 整个生命周期的监听
        ProcessLifecycleOwner.get().lifecycle.addObserver(ApplicationObserver())
        ShareFlowBus.initApplicaiton(this)
        UMUtils.preInit(this, "629f1f7205844627b5a49717", "kotlinlearn")
        UMUtils.init(
            this,
            "629f1f7205844627b5a49717",
            "kotlinlearn",
            "vriyilm4k8wlks3kwffydmxv1ytw0zo8",
        ).autoCollect()
        StoreImp.init(applicationContext, StoreConfig(type = StoreType.SharePre))
    }
}
