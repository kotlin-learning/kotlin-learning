package com.timi.kotlinlearn.base

import android.app.Activity

/**
 * @Author: create by timi$
 * @time : create by 2021年11月21日18:31:56$
 * @function: 所有Acitivty的基类
 *  对于新建的Acitvity 如果需要传递参数请使用 companion object 定义好启动Activity的参数如DemoActivity所示
 */
open class BaseActivity : Activity()
