package com.timi.kotlinlearn.kotlin

/**
 * @Author: create by timi
 * @time : create by 2021年12月1日19:32:29
 * @function: 用于演示Map filter的方法
 *
 */
class MapDemo {
    // test
    fun testMap() {
        println(filterDemo())
        println(mapDemo())
    }

    /**
     * 集合 map 以一定规则进行转换如：uppercase全部转换成大写
     */
    fun mapDemo(): List<String> {
        val fruites = listOf("apple", "banana", "Orange", "Pear")
        val map = fruites.map {
            it.uppercase()
        }
        return map
    }

    /**
     *集合以一定规则过滤掉其中的元素 如it.length<5 长度小于5则保留
     */
    fun filterDemo(): List<String> {
        val fruites = listOf("apple", "banana", "Orange", "Pear")
        val map = fruites
            .filter { it.length < 5 }
            .map {
                it.uppercase()
            }
        return map
    }
}
