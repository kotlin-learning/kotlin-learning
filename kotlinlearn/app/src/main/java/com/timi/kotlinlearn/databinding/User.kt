package com.timi.kotlinlearn.databinding

import androidx.databinding.ObservableField

/**
 *  用于演示DataBing的User类
 *  @author lifeng.xing timi
 *  created at 2021/12/28  14:46
 */
data class User(val name: String = "姓名", val sex: String = "性别", val age: Int = 0, var remark: ObservableField<String>) {
    // 整体整个的信息
    fun UserInfo(): String {
        return "姓名：$name 性别：$sex 年龄：$age remark:${remark.get()}"
    }
}
