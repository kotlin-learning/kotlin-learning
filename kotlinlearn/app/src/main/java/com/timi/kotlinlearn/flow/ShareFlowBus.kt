package com.timi.kotlinlearn.flow

import android.app.Application
import androidx.lifecycle.* // ktlint-disable no-wildcard-imports

/**
 *  使用ShareFlow实现EventBus
 *  @author lifeng.xing timi
 *  created at 2021/12/10  9:54
 */
object ShareFlowBus : ViewModelStoreOwner {
    lateinit var application: Application
    private val eventViewModelStore: ViewModelStore = ViewModelStore()

    /**
     * 初始化application
     */
    fun initApplicaiton(app: Application) {
        application = app
    }

    private val mApplicationProvider: ViewModelProvider by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application),
        )
    }

    fun <T : ViewModel> getApplicationScopeViewModel(modelClass: Class<T>): T {
        return mApplicationProvider[modelClass]
    }

    override fun getViewModelStore(): ViewModelStore {
        return eventViewModelStore
    }
}
