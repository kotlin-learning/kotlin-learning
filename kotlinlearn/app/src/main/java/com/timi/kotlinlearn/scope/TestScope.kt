package com.timi.kotlinlearn.scope

import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

class ViewModelScope

/**
 * @ClassName: 测试Scope
 * @Description:
 * @author: lifeng xing
 * @date: 2023/4/19
 */
fun main() {
    val measureTimeMillis = measureTimeMillis {
        runBlocking {
            val asyncA = async(start = CoroutineStart.LAZY) {
                delay(3000)
                1
            }
            asyncA.start()
            val asyncB = async(start = CoroutineStart.LAZY) {
                delay(4000)
                2
            }
            asyncB.start()
            println("asyncA" + "=" + asyncA.await())
            println("asyncB" + "=" + asyncB.await())
        }
    }
    println("measureTimeMillis=$measureTimeMillis")
    val measureTimeMillis4 = measureTimeMillis {
        runBlocking {
            val asyncA = async(start = CoroutineStart.DEFAULT) {
                delay(3000)
                1
            }
            asyncA.start()
            val asyncB = async(start = CoroutineStart.DEFAULT) {
                delay(4000)
                2
            }
            asyncB.start()
            println("asyncA" + "=" + asyncA.await())
            println("asyncB" + "=" + asyncB.await())
        }
    }
    println("measureTimeMillis=$measureTimeMillis4")
    // try catch
    measureTimeMillis {
        runBlocking {
            val job = launch {
                try {
                    repeat(1000) { i ->
                        println("job: I'm sleeping $i ...")
                        delay(500L)
                    }
                } catch (e: CancellationException) {
                    println("CancellationException ${e.message}")
                } finally {
                    println("job: I'm running finally")
                }
            }
            delay(1300L)
            println("main: I'm tired of waiting!")
            job.cancelAndJoin()
            println("main: Now I can quit.")
        }
    }
}
