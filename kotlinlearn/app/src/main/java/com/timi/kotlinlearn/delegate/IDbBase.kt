package com.timi.kotlinlearn.delegate

/**
 * @ClassName: IDbBase
 * @Description: 数据库操作的接口
 * @author: lifeng xing
 * @date: 2023/4/26
 */
interface IDbBase<D> {
    fun update(d: D)
    fun inert(d: D)
    fun del(d: D)
    fun query(d: D)
    fun queryAll()
}
