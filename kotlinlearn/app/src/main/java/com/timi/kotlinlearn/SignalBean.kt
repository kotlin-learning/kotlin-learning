package com.timi.kotlinlearn

/**
 * @Author: create by timi
 * @time : create by 2021年11月24日19:58:37
 * @function: wifi 流量 信号强度
 *
 */
data class SignalBean(var level: Int, var isNetEnable: Boolean, var isWifi: Boolean)
