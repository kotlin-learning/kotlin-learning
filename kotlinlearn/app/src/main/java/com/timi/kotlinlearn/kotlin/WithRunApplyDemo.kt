package com.timi.kotlinlearn.kotlin

import java.lang.StringBuilder

/**
 * @Author: create by timi$
 * @time : create by 2021年11月21日19:16:49$
 * @function: with run apply
 *
 */
class WithRunApplyDemo {
    /**
     *  with的一般结构
     *  with函数不是以扩展的形式存在的。它是将某对象作为函数的参数，
     *  在函数块内可以通过 this 指代该对象。
     *  返回值为函数块的最后一行或指定return表达式。
     *  with(object){
     *     //todo
     *  }
     */
    fun StringBuilderWith(): String {
        val fruites = listOf("apple", "banana", "Orange", "Pear")
        // with传入对象 返回的是最后一行/return的内容
        var result = with(StringBuilder()) {
            // 可使用this也可以直接不写代替
            append("start eating fruites. \n")
            for (fruite in fruites) {
                append(fruite).append("\n")
            }
            append("Ate all fruites.")
            // 最后一行作为函数的返回值也可以直接return 返回
            toString()
        }
        return result
    }
    /**
     * run 和with 类似唯一的不同之处在于 run是通过对象调用run方法 而with是将对象作为参数传入
     * object.run{
     *     //todo
     *   }
     */
    fun StringBuilderRun(): String {
        val fruites = listOf("apple", "banana", "Orange", "Pear")
        // 通过对象调用run方法 返回的是最后一行/return的内容
        var result = StringBuilder().run {
            // 可使用this也可以直接不写代替
            append("start eating fruites. \n")
            for (fruite in fruites) {
                append(fruite).append("\n")
            }
            append("Ate all fruites.")
            // 最后一行作为函数的返回值也可以直接return 返回
            toString()
        }
        return result
    }
    /**
     * apply和run很类似唯一的区别在于aplly返回的是对象本身 而with返回的是return的内容/最后一行的内容
     */
    fun StringBuilderApply(): String {
        val fruites = listOf("apple", "banana", "Orange", "Pear")
        // 通过对象调用apply方法 返回的是对象本身
        var result = StringBuilder().apply {
            // 可使用this也可以直接不写代替
            append("start eating fruites. \n")
            for (fruite in fruites) {
                append(fruite).append("\n")
            }
            append("Ate all fruites.")
            // 最后一行作为函数的返回值也可以直接return 返回
            toString()
        }
        return result.toString()
    }
}
