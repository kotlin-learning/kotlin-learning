package com.timi.kotlinlearn.event

data class ServiceEvent(val key: String, val value: String)
