package com.timi.kotlinlearn.base

import android.app.Activity

/**
 * @Author: create by timi$
 * @time : create by 2021年11月21日18:20:12$
 * @function:  AcitvityController activity控制器 用于存储Acitivity和一键退出
 *
 */
object AcitvityController {
    private val activities = ArrayList<Activity>()
    /**
     * 添加Acitivity
     */
    fun addActivity(act: Activity) {
        activities.add(act)
    }

    /**
     * 移除Acitivity
     */
    fun removeActivity(act: Activity) {
        activities.remove(act)
    }

    /**
     * 关闭所有Activity 退出
     */
    fun finishAll() {
        for (act in activities) {
            if (!act.isFinishing) {
                act.finish()
            }
        }
        activities.clear()
        // 直接杀死当前应用的进行，酌情添加
//        android.os.Process.killProcess(android.os.Process.myPid())
    }
}
