package com.timi.kotlinlearn.scope

import kotlinx.coroutines.*

/**
 * 协程异常处理
 *   1、try catch 简单粗暴直接捕获
 *   2、协程之间的关系
 *      多个并发操作：多个需要同时处理的任务
 *      结构化并发操作：CoroutineScope中通过launch启用一个子任务，当父协程取消时，其所有子协程也都会关闭，
 *      单独取消一个子协程并不会影响其他子协程的执行
 *   3、异常流程：当子协程crash时，会传递给父协程，且取消掉子协程和父协程
 *      1）先 cancel 子协程
 *      2）取消自己
 *      3）将异常传递给父协程
 *      4）(重复上述过程,直到根协程关闭)
 *   ***********************************************************************************************
 *   4、supervisorJob 是一个特殊的Job,其会改变异常的传递方式，当使用它时,我们子协程的失败不会影响到其他子协程与父协程，
 *      通俗点理解就是:子协程会自己处理异常，并不会影响其兄弟协程或者父协程
 *      SupervisorJob 可以用来改变我们的协程异常传递方式，从而让子协程自行处理异常。
 *      但需要注意的是，因为协程具有结构化的特点，SupervisorJob 仅只能用于同一级别的子协程。
 *      如果我们在初始化 scope 时添加了 SupervisorJob ,那
 *      么整个scope对应的所有 根协程 都将默认携带 SupervisorJob ，否则就必须在 CoroutineContext 显示携带 SupervisorJob。
 *      如：
 *        val scope = CoroutineScope(CoroutineExceptionHandler { _, _ -> })
 *        scope.launch {
 *            launch(CoroutineName("A") + SupervisorJob()) {
 *                  delay(10)
 *                  throw RuntimeException()
 *                  }
 *            launch(CoroutineName("B")) {
 *                 delay(200)
 *                 Log.e("petterp", "猜猜我还能不能打印")
 *                }
 *            }
 *   ***********************************************************************************************
 */
fun main() {
//    testStructuring()
//    rightExpHandler()
    testSupervisorJob()
}

/**
 * SupervisorJob
 * supervisorJob 是一个特殊的Job,其会改变异常的传递方式，当使用它时,我们子协程的失败不会影响到其他子协程与父协程，
 * 通俗点理解就是:子协程会自己处理异常，并不会影响其兄弟协程或者父协程
 * 子线程自己处理异常 不影响父协程 其他子协程
 *  需要在activity/fragment中验证 否则不会打印(testSupervisorJob:正常执行，不会受到影响)
 *  tip:CoroutineScope(SupervisorJob() + CoroutineExceptionHandler { coroutineContext, throwable -> })
 *      直接以"+"连接是因为其父类重写了plus方法
 */
fun testSupervisorJob() {
    println("testSupervisorJob:开始")
    val coroutineScope =
        CoroutineScope(SupervisorJob() + CoroutineExceptionHandler { coroutineContext, throwable -> })
    coroutineScope.launch(CoroutineName("A")) {
        delay(10)
        println("testSupervisorJob:抛出异常")
        throw RuntimeException()
    }
    coroutineScope.launch(CoroutineName("B")) {
        delay(100)
        println("testSupervisorJob:正常执行，不会受到影响")
    }
    println("testSupervisorJob:结束")
}

/**
 * 正常处理异常Handler
 */
fun rightExpHandler() {
    // 1. 初始化scope时
    val scope = CoroutineScope(Job() + CoroutineExceptionHandler { _, _ -> })
    // 2. 根协程 根协程捕获异常
    scope.launch(CoroutineExceptionHandler { _, _ -> }) { }
}

/***
 * 测试结构化 crash
 */
fun testStructuring() {
    runBlocking {
        println("父协程：执行开始")
        launch {
            println("子协程：A执行开始")
            delay(10)
            println("子协程：A执行结束")
            throw NullPointerException()
        }
        launch {
            println("子协程：B执行开始")
            delay(10)
            println("子协程：B执行结束")
        }
        delay(100)
        println("父协程：执行结束")
    }
}
