package com.timi.kotlinlearn.kotlin

/**
 * @Author: create by timi
 * @time : create by 2021年11月23日20:09:05
 * @function: 重载的方法 用于对象內重载某些方法  更方便开发
 *
 */
class OperatorDemo {
    fun OperatorAdd() {
        val money3 = Money(1) + Money(2)
        println(money3.num)
        val money4 = Money(1) + 2
        println(money4.num)
    }
}

/**
 * 语法糖表达式和实际调用函数对比
 *  a+b    a.plus(b)
 *  a-b    a.minus(b)
 *  a*b    a.times(b)
 *  a/b    a.div(b)
 *  a%b    a.rem(b)
 *  a++    a.inc()
 *  a--    a.dec
 *  +a     a.unaryPlus
 *  -a     a.unaryMinus
 *  !a     a.not()
 *  a==b     a.equals(b)
 *  a..b     a.rangeTo(b)
 *  a[b]     a.get(b)
 *  a[b]=c     a.set(b,c)
 *  a in b      b.contains(a)
 */
class Money(var num: Int) {
    /**
     * 重载 + 的方法 这样就可以实现2个Money对象进行+
     * @param money Money对象
     */
    operator fun plus(money: Money): Money {
        return Money(num + money.num)
    }

    /**
     * 重载 + 的方法 这样就可以实现对象Money和一个Int进行相加返回对象Money
     * @param num 一个钱的数字
     */
    operator fun plus(num: Int): Money {
        return Money(num + num)
    }
}
