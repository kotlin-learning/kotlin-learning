package com.timi.kotlinlearn.dialog

import android.content.Context
import android.content.DialogInterface
import androidx.annotation.CallSuper
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.timi.kotlinlearn.R

/**
 *  用于替换Dialog
 *  @author lifeng.xing timi
 *  created at 2021/12/22  17:18
 */
open class BaseDialogFragment : DialogFragment(), DialogInterceptor {
    // 执行的是Commit 方法 最终也是通过Handler的方式
    override fun show(manager: FragmentManager, tag: String?) {
        // 解决bug：Android java.lang.IllegalStateException: Fragment already added
        // 在每个add事务前增加一个remove事务，防止连续的add
        manager.beginTransaction().remove(this).commit()
        super.show(manager, tag)
    }
    private var mChain: DialogChain? = null
    /**
     * 下一个拦截器 用于当Dialog 消失时调用下一个Dialog
     */
    fun chain(): DialogChain? = mChain

    @CallSuper
    override fun intercept(chain: DialogChain) {
        mChain = chain
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mChain?.process()
    }

    /**
     * 在OnStart()中 设置dialog的大小
     */
    override fun onStart() {
        super.onStart()
        val dialog = dialog

        if (dialog != null && null != context) {
            context?.let {
                // 设置宽高
                dialog.window
                    ?.setLayout(dp2px(it, 540F), dp2px(it, 375F))
            }
            val params = dialog.window!!.attributes
            // 不加此动画，dialog背景会突然变暗 o(╯□╰)o
            params.windowAnimations = R.style.makesureDialog
            dialog.window!!.attributes = params
        }
    }

    fun dp2px(context: Context, dipValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dipValue * scale + 0.5f).toInt()
    }

    override fun getShowsDialog(): Boolean {
        return super.getShowsDialog()
    }
}
