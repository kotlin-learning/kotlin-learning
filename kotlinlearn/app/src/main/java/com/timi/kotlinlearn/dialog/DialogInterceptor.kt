package com.timi.kotlinlearn.dialog

/**
 *  Dialog的拦截器 每一个节点之前的拦截器
 *  @author lifeng.xing timi
 *  created at 2021/12/22  17:09
 */
interface DialogInterceptor {
    // 拦截方法
    fun intercept(chain: DialogChain)
}
