package com.timi.kotlinlearn.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update

/**
 * @Author: create by timi
 * @time : create by 2021年12月5日18:40:19
 * @function:  Room操作数据库的基类
 *
 */
public interface BaseDao<T> {
    // 插入单个数据
    @Insert
    fun insertItem(t: T)

    // 插入list数据
    @Insert
    fun insertItems(items: List<T>?)

    // 删除item
    @Delete
    fun deleteItem(item: T)

    // 更新item
    @Update
    fun updateItem(item: T)
}
