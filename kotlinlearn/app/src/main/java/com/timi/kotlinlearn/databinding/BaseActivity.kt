package com.timi.kotlinlearn.databinding

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity

/**
 *  用于DataBinding的基类
 *  @author lifeng.xing timi
 *  created at 2021/12/29  13:48
 */
abstract class BaseActivity<VB : ViewDataBinding>() : FragmentActivity() {
    // 用于子类调用
    internal val mBinding: VB by lazy(mode = LazyThreadSafetyMode.NONE) {
        getViewBinding(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mBinding.root)
    }
}
