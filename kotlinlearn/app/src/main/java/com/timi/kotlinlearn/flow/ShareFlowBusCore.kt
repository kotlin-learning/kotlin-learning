package com.timi.kotlinlearn.flow

import android.util.Log
import androidx.lifecycle.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 *  核心库 继承自ViewModel 使用viewModelScope 协程
 *  @author lifeng.xing timi
 *  created at 2021/12/10  13:16
 */
class ShareFlowBusCore : ViewModel() {
    /**
     * 正常的Event事件
     */
    private val eventFlows: HashMap<String, MutableSharedFlow<Any>> = HashMap()

    /**
     * 粘性事件
     */
    private val stickyEventFlows: HashMap<String, MutableSharedFlow<Any>> = HashMap()

    /**
     * 获取EventFlow
     * 1、区分粘性事件和非粘性事件
     * 2、如果为空则初始化一个MutableSharedFlow
     * 3、添加到事件的HashMap里面
     */
    private fun getEventFlow(eventName: String, isSticky: Boolean): MutableSharedFlow<Any> {
        return if (isSticky) {
            stickyEventFlows[eventName]
        } else {
            eventFlows[eventName]
        } ?: MutableSharedFlow<Any>(
            // 当新的订阅者Collect时，发送几个已经发送过的数据给它
            // 发送1个或者不发送事件，粘性和非粘性的区分
            replay = if (isSticky) 1 else 0,
            // 减去replay，MutableSharedFlow还缓存多少数据
            extraBufferCapacity = Int.MAX_VALUE
        ).also {
            if (isSticky) {
                // 添加事件
                stickyEventFlows[eventName] = it
            } else {
                // 添加事件
                eventFlows[eventName] = it
            }
        }
    }

    /**
     * 监听事件
     * @param lifecycleOwner
     * @param eventName
     * @param instate 绑定的生命周期回调
     * 如：STARTED 其在onStart和onStop之间可用 发生在生命周期启动时，在生命周期停止时停止收集
     * @param dispatcher 线程
     * @param isSticky 是否是粘性事件
     * @param onReceive 回调
     */
    fun <T : Any> observeEvent(
        lifecycleOwner: LifecycleOwner,
        eventName: String,
        instate: Lifecycle.State,
        dispatcher: CoroutineDispatcher,
        isSticky: Boolean,
        onReceive: (T) -> Unit
    ) {
        Log.i("ShareFlowBusCore", "observeEvent eventName=$eventName")
        lifecycleOwner.lifecycleScope.launch {
            lifecycleOwner.lifecycle.whenStateAtLeast(instate) {
                getEventFlow(eventName, isSticky).collect {
                    Log.i("ShareFlowBusCore", "collect eventName=$eventName it=$it")
                    this.launch(dispatcher) {
                        invokeReceived(it, onReceive)
                    }
                }
            }
        }
    }

    /**
     *
     *
     */
    suspend fun <T : Any> observeWithoutLifecycle(
        eventName: String,
        isSticky: Boolean,
        onReceive: (T) -> Unit
    ) {
        getEventFlow(eventName, isSticky).collect {
            invokeReceived(it, onReceive)
        }
    }

    /**
     * 删除事件
     */
    fun removeStickEvent(eventName: String) {
        stickyEventFlows.remove(eventName)
    }

    /**
     * 清空事件
     */
    @ExperimentalCoroutinesApi
    fun clearStickEvent(eventName: String) {
        stickyEventFlows[eventName]?.resetReplayCache()
    }

    /**
     * 发送事件
     * @param eventName MutableSharedFlow对应的key
     * @param value 发送的内容
     * @param timeMillis 延时
     */
    fun postEvent(eventName: String, value: Any, timeMillis: Long) {
        listOfNotNull(
            getEventFlow(eventName, false),
            getEventFlow(eventName, true)
        ).forEach { flow ->
            Log.i("ShareFlowBusCore", "postEvent eventName=$eventName value=$value")
            viewModelScope.launch {
                delay(timeMillis)
                flow.emit(value)
            }
        }
    }
    /**
     * 强转value->T并且回调回去
     */
    private fun <T : Any> invokeReceived(value: Any, onReceived: (T) -> Unit) {
        try {
            onReceived.invoke(value as T)
        } catch (e: ClassCastException) {
            Log.e("ShareFlowBusCore", "ClassCastException")
        } catch (e: Exception) {
            Log.e("ShareFlowBusCore", "Exception")
        }
    }
}
