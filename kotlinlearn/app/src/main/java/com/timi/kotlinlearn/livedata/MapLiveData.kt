package com.timi.kotlinlearn.livedata

import androidx.lifecycle.* // ktlint-disable no-wildcard-imports
import com.timi.kotlinlearn.SignalBean

/**
 * @Author: create by timi
 * @time : create by 2021年12月5日20:34:58
 * @function: 用于演示map和switchmap
 * 我们通常在Viewmodel里面使用livedata 对于livedata的转换也在viewmodel里面
 * map只对于livedata的一个数值的变换
 * switchmap则是对livedata进行一个数值的触发当livedata的数值满足某一个条件是返回一个livedata
 */
class MapLiveData : ViewModel() {

    private var signalLiveData: MutableLiveData<SignalBean> = MutableLiveData<SignalBean>()

    // 数值的转换
    private val singnallLiveDataMap: LiveData<Int> = signalLiveData.map {
        it.level
    }

    // 数值的转换
    private val singnallLiveDataMapByTransformations: LiveData<Int> =
        Transformations.map(signalLiveData) {
            it.level
        }

    // 返回一个livedata
    private val singnallLiveDataSwitchMapByTransformations: LiveData<Int> =
        Transformations.switchMap(signalLiveData) {
            if (it.isWifi) {
                signalLiveDataA()
            } else {
                signalLiveDataB()
            }
        }

    // 返回一个livedata
    private val singnallLiveDataSwitchMapByOwn: LiveData<Int> = signalLiveData.switchMap() {
        if (it.isWifi) {
            signalLiveDataA()
        } else {
            signalLiveDataB()
        }
    }

    /**
     * 返回一个Int的Livedata
     */
    private fun signalLiveDataA(): LiveData<Int> {
        return MutableLiveData<Int>()
    }

    private fun signalLiveDataB(): LiveData<Int> {
        return MutableLiveData<Int>()
    }

    class People {
        val name = ""
        var age = 0
            get() = field
            set(value) {
                field = value
            }
    }
}
