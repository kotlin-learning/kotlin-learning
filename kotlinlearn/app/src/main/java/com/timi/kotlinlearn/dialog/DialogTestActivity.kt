package com.timi.kotlinlearn.dialog

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.timi.kotlinlearn.R

/**
 *  用于测试多弹出框的界面
 *  @author lifeng.xing timi
 *  created at 2021/12/23  15:51
 */
class DialogTestActivity : FragmentActivity() {
    private lateinit var dialogChain: DialogChain

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog_test)
        val updateDialogFragment = UpdateDialogFragment.newInstance()
        updateDialogFragment.setNeedToUpdate()
        dialogChain = DialogChain.create(3).addInterceptor(FullScreenDialogFragment.newInstance())
            .addInterceptor(TopTipDialog.newInstance())
            .addInterceptor(updateDialogFragment)
            .attach(this)
            .build()
    }

    fun testDialog(view: android.view.View) {
        dialogChain.process()
    }
}
