package com.timi.kotlinlearn.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.timi.kotlinlearn.R

class AdDialogFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ad_dialog, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            AdDialogFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
