package com.timi.kotlinlearn.base

import android.content.Context
import android.content.Intent

/**
 * @Author: create by timi$
 * @time : create by 2021年11月21日18:33:47$
 * @function: 用于启动Acitivity的示例
 *
 */
class DemoActivity : BaseActivity() {
    companion object {
        fun activityStart(context: Context, name: String, sex: String) {
            context.startActivity(Intent().putExtra("name", name).putExtra("sex", sex))
        }
    }
}
