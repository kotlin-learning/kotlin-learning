package com.timi.kotlinlearn.lifecycle

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner

/**
 * @Author: create by timi
 * @time : create by 2021年12月1日20:15:35
 * @function: 实现LifecycleEventObserver 回调宿主生命周期事件封装成 Lifecycle.Event
 *
 */
class FragmentLifeEventObeserver : LifecycleEventObserver {
    val TAG = "FragmentLifeEventObeserver"
    @SuppressLint("LongLogTag")
    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_CREATE -> {
                Log.i(TAG, "Lifecycle.Event.ON_CREATE")
            }
            Lifecycle.Event.ON_RESUME -> {
                Log.i(TAG, "Lifecycle.Event.ON_RESUME")
            }
            Lifecycle.Event.ON_START -> {
                Log.i(TAG, "Lifecycle.Event.ON_START")
            }
            Lifecycle.Event.ON_PAUSE -> {
                Log.i(TAG, "Lifecycle.Event.ON_PAUSE")
            }
            Lifecycle.Event.ON_STOP -> {
                Log.i(TAG, "Lifecycle.Event.ON_STOP")
            }
            Lifecycle.Event.ON_DESTROY -> {
                Log.i(TAG, "Lifecycle.Event.ON_DESTROY")
            }
        }
    }
}
