package com.timi.kotlinlearn.lifecycle

import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner

/**
 * @Author: create by timi
 * @time : create by 2021年12月1日19:59:03
 * @function: 用于监听Application 整个生命周期的回调
 *
 */
open class ApplicationObserver : DefaultLifecycleObserver {
    val TAG = "ApplicationObserver"
    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        Log.i(TAG, "ApplicationObserver onCreate")
    }

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        Log.i(TAG, "ApplicationObserver onStart")
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        Log.i(TAG, "ApplicationObserver onResume")
    }

    override fun onPause(owner: LifecycleOwner) {
        super.onPause(owner)
        Log.i(TAG, "ApplicationObserver onPause")
    }

    override fun onStop(owner: LifecycleOwner) {
        super.onStop(owner)
        Log.i(TAG, "ApplicationObserver onStop")
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        Log.i(TAG, "ApplicationObserver onDestroy")
    }
}
