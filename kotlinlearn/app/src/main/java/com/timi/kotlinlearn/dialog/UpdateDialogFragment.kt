package com.timi.kotlinlearn.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.timi.kotlinlearn.R

/**
 *  首页升级的弹出框（优先级最高）
 *  @author lifeng.xing timi
 *  created at 2021/12/23  15:59
 */
class UpdateDialogFragment : BaseDialogFragment() {
    private lateinit var tvCancel: TextView
    private lateinit var tvUpDate: TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_dialog_update, container, false)
        tvCancel = inflate.findViewById(R.id.tv_cancel)
        tvUpDate = inflate.findViewById(R.id.tv_update)
        tvUpDate.setOnClickListener {
            // TODO: 2021/12/23 做更新下载。。。
            Toast.makeText(context, "正在更新...", Toast.LENGTH_SHORT).show()
            dismiss()
        }
        tvCancel.setOnClickListener {
            // 弹出下个dialog
            Toast.makeText(context, "取消更新...", Toast.LENGTH_SHORT).show()
            dismiss()
        }
        return inflate
    }
    companion object {
        @JvmStatic
        fun newInstance() =
            UpdateDialogFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
    // 是否需要更新
    private var isNeedUpdate = false

    override fun intercept(chain: DialogChain) {
        super.intercept(chain)
        // 这里可根据实际业务场景来定制dialog 显示条件。
        if (isNeedUpdate) {
            // show dialog
            chain.activity?.supportFragmentManager?.let { show(it, "TopTipDialog") }
        } else {
            // 当自己不具备弹出条件的时候，可以立刻把请求转交给下一个拦截器。
            chain.process()
        }
    }
    // 设置需要更新 链式调用
    fun setNeedToUpdate(): UpdateDialogFragment {
        isNeedUpdate = true
        return this
    }
    // dialog 消失重置状态
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        isNeedUpdate = false
    }
}
