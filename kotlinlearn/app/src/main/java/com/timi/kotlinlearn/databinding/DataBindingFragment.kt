package com.timi.kotlinlearn.databinding

import android.os.Bundle
import android.view.View

/**
 *  DataBinding 的Fragment
 *  @author lifeng.xing timi
 *  created at 2021/12/29  13:47
 */
class DataBindingFragment : BaseFragment<FragmentDatabindingBinding>() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.tvFragmentTest.text = "测试DataBindingFragment"
    }
}
