package com.timi.kotlinlearn

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatSpinner
import com.timi.kotlincore.store.StoreImp
import com.timi.kotlincore.store.StoreType

/**
 * @ClassName: 测试mmkv  sharepre datasource 存储键值对
 * @Description:
 * @author: lifeng xing
 * @date: 2022/8/3
 */
class StoreActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    lateinit var spinner: AppCompatSpinner
    lateinit var tvAll: TextView
    var index = 0
    lateinit var datas: MutableMap<String, *>

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("Test", Test.result)
        Test.index++
        Log.i("Test", Test.result)
        Log.i("TestKT", TestKT.result)
        TestKT.index++
        Log.i("TestKT", TestKT.result)
        Log.i("TestKT", "getResultStr=" + TestKT.getResultStr())
        setContentView(R.layout.activity_store)
        spinner = findViewById(R.id.sp_type)
        tvAll = findViewById(R.id.tv_save_value)
        // adapter
        ArrayAdapter.createFromResource(
            this,
            R.array.store_type,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        // 选项
        spinner.onItemSelectedListener = this

        findViewById<Button>(R.id.bt_add).setOnClickListener {
            StoreImp.save("key_$index", "value_$index")
            index++
            findViewById<Button>(R.id.bt_getall).performClick()
        }
        findViewById<Button>(R.id.bt_mig).setOnClickListener {
            StoreImp.migration(datas)
        }
        findViewById<Button>(R.id.bt_del).setOnClickListener {
            index--
            StoreImp.remove("key_$index")
            findViewById<Button>(R.id.bt_getall).performClick()
        }
        findViewById<Button>(R.id.bt_getall).setOnClickListener {
            var result = StringBuffer()
            StoreImp.getAll().forEach {
                result.append("${it.key} = ${it.value} \n")
            }
            tvAll.text = result
        }
        findViewById<Button>(R.id.bt_clear).setOnClickListener {
            index = 0
            StoreImp.clearAll()
            findViewById<Button>(R.id.bt_getall).performClick()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        datas = StoreImp.getAll()
        when (position) {
            0 -> StoreImp.swichStore(this, StoreType.SharePre)
            1 -> StoreImp.swichStore(this, StoreType.MMKV)
            2 -> StoreImp.swichStore(this, StoreType.DataStore)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        datas = StoreImp.getAll()
        StoreImp.swichStore(this, StoreType.SharePre)
    }
}
