package com.timi.kotlinlearn.databinding

import androidx.lifecycle.ViewModel
/**
 *  DataBinding的ViewModel
 *  @author lifeng.xing timi
 *  created at 2021/12/30  10:49
 */
class DataBindingViewModel : ViewModel()
