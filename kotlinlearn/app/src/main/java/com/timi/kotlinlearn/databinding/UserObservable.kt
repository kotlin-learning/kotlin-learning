package com.timi.kotlinlearn.databinding

import androidx.databinding.ObservableArrayMap
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField

/**
 *  可观察字段的User 可观察的属性
 *  @author lifeng.xing timi
 *  created at 2021/12/30  10:50
 */
class UserObservable {
    public val name = ObservableField<String>().apply {
        set("邢力丰")
    }
    private val age = ObservableField<Int>().apply {
        set(18)
    }
    private val high = ObservableField<Short>().apply {
        set(168)
    }
    // ObservableBoolean
    // ObservableByte
    // ObservableChar
    // ObservableShort
    // ObservableInt
    // ObservableLong
    // ObservableFloat
    // ObservableDouble
    // ObservableParcelable
    // 是否结婚
    private val isMerry = ObservableBoolean().apply {
        set(false)
    }
    // 可观察的array map
    public val teachers = ObservableArrayMap<String, Any>().apply {
        put("teach", "语文老师")
        put("age", 22)
        put("sex", "女")
    }

    override fun toString(): String {
        return "UserObservable(name=$name, age=$age, high=$high, isMerry=$isMerry, teachers=$teachers)"
    }
}
