package com.timi.kotlinlearn.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ServiceLifecycleDispatcher
import androidx.lifecycle.lifecycleScope
import com.timi.kotlinlearn.event.ServiceEvent
import com.timi.kotlinlearn.flow.observeEvent

class ShareFlowTestService : Service(), LifecycleOwner {
    private val mDispatcher = ServiceLifecycleDispatcher(this)
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.i("ShareFlowTestService", "onCreate()")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        Log.i("ShareFlowTestService", "onStartCommand()")
        observeEvent<ServiceEvent>(lifecycleScope) {
            Log.i("ShareFlowTestService", "observeEvent=$it")
        }
        return super.onStartCommand(intent, flags, startId)
    }
    override fun getLifecycle(): Lifecycle {
        return mDispatcher.lifecycle
    }
}
