package com.timi.kotlinlearn.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.timi.kotlinlearn.lifecycle.FragmentLifeEventObeserver
import com.timi.kotlinlearn.lifecycle.FragmentLifeObeserver

/**
 * @Author: create by timi
 * @time : create by 2021年12月1日19:40:15
 * @function: Fragment的基类 open表示可继承
 * Tip1: 由于onActivityCreated已过期
 *  可在onCreateView中初始化view
 *  而在onViewCreated进行一些赋值操作
 * Tip2:如果需要监听Fragment的Activity的onCreate等方法的回调可以使用
 * requireActivity().lifecycle.addObserver 增加其生命周期的监听器
 * 1: 实现DefaultLifecycleObserver 回调响应的生命周期方法需要引入lifecycle-common的依赖
 *    implementation("androidx.lifecycle:lifecycle-common-java8:2.4.0")
 * 2: 实现LifecycleEventObserver   回调 宿主生命周期事件封装成 Lifecycle.Event
 * 3: 第三种就是注解的方式不推荐使用 实现LifecycleObserver接口
 *     @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)注解的形式
 */
open class BaseFragment : Fragment() {
    val TAG = "BaseFragment"
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // init view
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // view 已经初始化 可进行一些赋值操作
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().lifecycle.addObserver(lifecycleEventObserver)
        requireActivity().lifecycle.addObserver(fragmentLifeObeserver)
    }
    // 通过LifecycleEventObserver 监听宿主Activity的回调
    private var lifecycleEventObserver = FragmentLifeEventObeserver()
    // 通过DefaultLifecycleObserver 监听宿主Activity的回调
    private var fragmentLifeObeserver = FragmentLifeObeserver()
    override fun onDetach() {
        super.onDetach()
        requireActivity().lifecycle.removeObserver(lifecycleEventObserver)
        requireActivity().lifecycle.removeObserver(fragmentLifeObeserver)
    }
}
