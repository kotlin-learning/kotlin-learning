package com.timi.kotlinlearn.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.timi.kotlinlearn.MainActivity
import com.timi.kotlinlearn.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity() {
    // binding
    private lateinit var binding: ActivitySplashBinding
    @RequiresApi(Build.VERSION_CODES.S)
    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
