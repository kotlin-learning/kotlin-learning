package com.timi.kotlinlearn

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.timi.kotlinlearn.event.ActivityEvent
import com.timi.kotlinlearn.event.AppEvent
import com.timi.kotlinlearn.flow.observeEvent
import com.timi.kotlinlearn.flow.postEvent

class TestShareFlow1Activity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_share_flow)
        observeEvent<AppEvent> {
            findViewById<TextView>(R.id.tv_msg).text = "接收到的消息：$it"
        }
        observeEvent<ActivityEvent>(this) {
            findViewById<TextView>(R.id.tv_msg).text = "接收到的消息：$it"
        }
    }

    fun sendEvent(view: android.view.View) {
        postEvent(this, ActivityEvent("ActivityEvent", "TestShareFlow1Activity ActivityEvent Test"))
    }
    fun sendEvent1(view: android.view.View) {
        postEvent(AppEvent("AppEvent", "TestShareFlow1Activity ActivityEvent Test"))
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun sendEvent2(view: android.view.View) {
        val intent = Intent(this, TestShareFlow2Activity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
    }
}
