package com.timi.kotlinlearn.databinding

import androidx.lifecycle.ViewModel
/**
 *  DataBinding的ViewModel 用于双向绑定
 *  @author lifeng.xing timi
 *  created at 2021/12/29  10:46
 */
class BindViewModel : ViewModel()
