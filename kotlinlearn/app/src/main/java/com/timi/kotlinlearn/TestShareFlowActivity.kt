package com.timi.kotlinlearn

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.timi.kotlinlearn.event.ActivityEvent
import com.timi.kotlinlearn.event.AppEvent
import com.timi.kotlinlearn.event.ServiceEvent
import com.timi.kotlinlearn.flow.observeEvent
import com.timi.kotlinlearn.flow.postEvent
import com.timi.kotlinlearn.service.ShareFlowTestService

/**
 *  用于测试ShareFlow
 *  @author lifeng.xing timi
 *  created at 2021/12/13  9:22
 */
class TestShareFlowActivity : AppCompatActivity() {
    val TAG = "TestShareFlowActivity"
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_share_flow)
        observeEvent<AppEvent> {
            findViewById<TextView>(R.id.tv_msg).text = "接收到的消息：$it"
        }
        observeEvent<ActivityEvent>(this) {
            findViewById<TextView>(R.id.tv_msg).text = "接收到的消息：$it"
        }
        observeEvent<ActivityEvent>(this) {
            Log.i(TAG, "observeEvent=$it")
            findViewById<TextView>(R.id.tv_msg).text = "接收到的消息：$it"
        }
        observeEvent<AppEvent> {
            Log.i(TAG, "observeEvent1=$it")
            findViewById<TextView>(R.id.tv_msg).text = "接收到的消息：$it"
        }
        observeEvent<AppEvent> {
            Log.i(TAG, "observeEvent2=$it")
            findViewById<TextView>(R.id.tv_msg).text = "接收到的消息：$it"
        }
        startService(Intent(applicationContext, ShareFlowTestService::class.java))
    }

    fun sendEvent(view: android.view.View) {
        postEvent(this, ActivityEvent("ActivityEvent", "TestShareFlowActivity ActivityEvent Test"))
    }
    fun sendEvent1(view: android.view.View) {
        postEvent(AppEvent("AppEvent", "TestShareFlowActivity ActivityEvent Test"))
    }
    fun sendServiceEvent(view: android.view.View) {
        postEvent(ServiceEvent("ServiceEvent", "TestShareFlowActivity ServiceEvent Test"))
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun sendEvent2(view: android.view.View) {
        val intent = Intent(this, TestShareFlow1Activity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
    }
}
