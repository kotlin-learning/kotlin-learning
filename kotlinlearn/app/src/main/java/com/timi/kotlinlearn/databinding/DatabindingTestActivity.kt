package com.timi.kotlinlearn.databinding

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.lifecycleScope
import com.timi.kotlinlearn.R

/**
 *  Databingd 测试Databinding
 *  @author lifeng.xing timi
 *  created at 2021/12/28  10:30
 */
class DatabindingTestActivity :
    BaseActivity<ActivityDatabindingTestBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 单向绑定 通过改变User改变UI的显示
        val user = User("timi", "男", 18, ObservableField("默认的备注"))
        mBinding.user = user
        // 无绑定 通过直接改变属性来改变UI的显示
        mBinding.sex = "男"
        // 无绑定 直接改变UI的内容
        mBinding.tvAge.text = "18"
        // 替换fragment
        supportFragmentManager.beginTransaction().replace(R.id.fm_test_frag, DataBindingFragment()).commit()
        lifecycleScope.launchWhenResumed {
            mBinding.userOb = UserObservable()
        }
        // 更改用户信息的备注 测试更改一条数据是否整体信息会跟着更改
        // 结果证明：单向绑定只会影响绑定相关字段的View的更改而不是整体进行更改
        mBinding.btnUpdateUser.setOnClickListener {
            user.remark.set("更改之后的备注")
        }
    }
}
