package com.timi.kotlinlearn.kotlinHighFunc

import java.lang.StringBuilder

/**
 * @Author: create by timi
 * @time : create by 2021年12月1日20:35:50
 * @function:
 *
 */
class Demo {
    fun testDemo() {
        // 参数名无关 只是传入一个方法作为参数
        example(::func)
        example(::func1)
        val fruites = listOf("apple", "banana", "Orange", "Pear")

        // 通过对象调用build方法 返回的是对象本身类似于Aplly
        var result = StringBuilder().build {
            // 可使用this也可以直接不写代替
            append("start eating fruites. \n")
            for (fruite in fruites) {
                append(fruite).append("\n")
            }
            append("Ate all fruites.")
            // 最后一行作为函数的返回值也可以直接return 返回
            toString()
        }
    }

    // 高阶函数 只传入一个方法作为参数
    fun example(func: (String, Int) -> Unit) {
        func("张三", 22)
    }

    fun func(name: String, age: Int) {
        println("$name 的年龄是 $age")
    }

    fun func1(name: String, high: Int) {
        println("$name 的身高是 $high")
    }

    // 扩展函数
    private fun StringBuilder.build(block: StringBuilder.() -> Unit) {
        block()
    }
}
