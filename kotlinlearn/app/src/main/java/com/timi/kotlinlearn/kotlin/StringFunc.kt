package com.timi.kotlinlearn.kotlin

import android.annotation.SuppressLint

/**
 * @ClassName: 一些已经定义好的kotlin 字符串方法
 * @Description:
 * @author: lifeng xing
 * @date: 2023/4/20
 */

@SuppressLint("SdCardPath")
fun main() {
    /******主要用于路径和文件的判断*******************************************************************/
    val learnPath = "/sdcard/data/com.timi.kotlinlearn/learn.doc"
    // 最后一个斜线出现之前的部分
    println(learnPath.substringBeforeLast("/"))
    // 最后一个斜线出现之后的部分
    println(learnPath.substringAfterLast("/"))
    // 最后一个点出现之后的部分
    println(learnPath.substringBeforeLast("."))
    // 最后一个点出现之后的部分
    println(learnPath.substringAfterLast("."))
    /******三重引号*********************************************************************************/
    // 1、可以省略转换符
    // 2、可以对字符串进行格式化
    val content = """ 
           关键的问题
           问题的关键
           以及问题哪里关键
    """.trimStart()
    println(content)
}
