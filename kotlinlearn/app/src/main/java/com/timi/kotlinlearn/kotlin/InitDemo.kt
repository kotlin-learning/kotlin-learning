package com.timi.kotlinlearn.kotlin

/**
 * @Author: create by timi$
 * @time : create by 2021年11月22日19:35:21$
 * @function:判断是否初始化了
 *
 */
class InitDemo {
    private lateinit var initString: String

    // 判断是否初始化了
    fun judgeInitStr() = !::initString.isInitialized
}
