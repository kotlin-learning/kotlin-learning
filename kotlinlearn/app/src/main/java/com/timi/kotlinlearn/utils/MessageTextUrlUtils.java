package com.timi.kotlinlearn.utils;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: MessageTextUrlUtils
 * @Description: 用于文本消息的链接处理类
 * @author: lifeng xing
 * @date: 2022/6/28
 */
public class MessageTextUrlUtils {
    private static MessageTextUrlUtils instance;

    public static MessageTextUrlUtils getInstance() {
        if(null==instance){
            instance=new MessageTextUrlUtils();
        }
        return instance;
    }
    public boolean isUrl(String str){
        return Pattern.compile(MessageTextUrlUtils.patternUrl).matcher(str).find();
    }
    /**
     * 判断字符串中是否有超链接，若有，则返回超链接。
     *
     * @param str
     * @return
     */
    public static String patternUrl = "((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|((www.|WWW.|Www.|WWw.|wwW.|wWW.)[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";

    public String getPatternUrl() {
        return patternUrl;
    }

    public String[] judgeString(String str) {
        Matcher m = Pattern.compile(patternUrl).matcher(str);
        String[] url = new String[str.length() / 5];
        int count = 0;
        while (m.find()) {
            count++;
            url[count] = m.group();
        }
        return url;
    }
    public void setTextUrl(final String[] url, String allUrl, TextView msgBodyText) {
        String dealAllUrl=allUrl;
        final ArrayList<UrlTextBean> beans = new ArrayList<>();
        //包含url
        if (url.length > 0) {
            SpannableString str = new SpannableString(allUrl);
            //遍历每一个url 并且设置响应的监听器
            for (int i = 0; i < url.length; i++) {
                if(!TextUtils.isEmpty(url[i])){
                    //第一次出现的位置
                    int start = dealAllUrl.indexOf(url[i])+allUrl.indexOf(dealAllUrl);
                    String[] split = dealAllUrl.split(url[i]);
                    if (split.length > 1&&start!=-1) {
                        int indexNextStart = dealAllUrl.indexOf(split[1])+allUrl.indexOf(dealAllUrl);
                        //找到响应的end位置
                        if (indexNextStart != -1) {
                            //改变allurl防止因为相同url 导致计算错误不能设置第二个url
                            dealAllUrl = dealAllUrl.substring(indexNextStart-allUrl.indexOf(dealAllUrl));
                            UrlTextBean bean = new UrlTextBean();
                            bean.url = url[i];
                            bean.start = start;
                            bean.end = indexNextStart;
                            beans.add(bean);
                        }
                    }else {
                        //防止只有一条url作为消息的情况
                        if(split.length==0||split.length==1){
                            UrlTextBean bean = new UrlTextBean();
                            bean.url = url[i];
                            bean.start = start;
                            bean.end = dealAllUrl.length();
                            beans.add(bean);
                        }
                    }
                }
            }
            //设置监听器
            for (int i = 0; i < beans.size(); i++) {
                final int finalI = i;
                str.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                        //点击事件

                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        //设置文本的颜色
                        ds.setColor(Color.parseColor("#FF03A9F4"));
                        //超链接形式的下划线，false 表示不显示下划线，true表示显示下划线
                        ds.setUnderlineText(true);
                    }
                }, beans.get(finalI).start, beans.get(finalI).end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            //不设置 没有点击事件
            msgBodyText.setMovementMethod(LinkMovementClickMethod.getInstance());
            msgBodyText.setText(str);
            //设置点击按下时的颜色为透明
            msgBodyText.setHighlightColor(Color.TRANSPARENT);
        }
    }
    /**
     * 用于处理每个Url 获取其对应的位置信息
     */
    static class UrlTextBean {
        String url;
        int start;
        int end;
    }
}
