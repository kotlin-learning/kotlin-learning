package com.timi.kotlinlearn

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import com.timi.kotlinlearn.databinding.DatabindingTestActivity
import com.timi.kotlinlearn.dialog.DialogTestActivity
import com.timi.permissionlib.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.*

class MainActivity : FragmentActivity(), EasyPermissionReqCallback, XXPermissionCallback {
    private val TAG: String = "MainActivity"
    private val mainViewModel by viewModels<MainViewModel>()
    private lateinit var createPermissionReq: EasyPermissionReq
    private lateinit var xxpermis: XXPermissionReq

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        createPermissionReq =
//            PermissionFactoryImp.getInstance()
//                .createPermissionReq(EasyPermissionReq::class.java) as EasyPermissionReq
//        createPermissionReq.configPermissionRequest(this, "应用需要使用摄像头权限！")
//                .requestPermission(
//                    this,
//                    Manifest.permission.CAMERA,
//                    Manifest.permission.WRITE_EXTERNAL_STORAGE
//                )
        xxpermis = PermissionFactoryImp.getInstance()
            .createPermissionReq(XXPermissionReq::class.java) as XXPermissionReq
        xxpermis.requestPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
        )
        val mutableLiveData = MutableLiveData<Int>()
        val arrayListOf = arrayListOf<Int>(1, 2, 3, 4, 5, 6)
        val maps = mapOf<Int, String>(Pair(1, "1"), Pair(2, "2"), Pair(3, "3"))
        arrayListOf.forEach {
            if (maps.keys.contains(it)) {
                mutableLiveData.value = it
                Log.i("test", "it=$it map=${maps.get(it)}")
            }
        }
    }

    fun startToShareFlow(view: android.view.View) {
        val intent = Intent(this, TestShareFlowActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
    }

    fun testDialog(view: android.view.View) {
        val intent = Intent(this, DialogTestActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
    }

    fun testDataBinding(view: android.view.View) {
        val intent = Intent(this, DatabindingTestActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
    }

    fun testStore(view: android.view.View) {
        val intent = Intent(this, StoreActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        createPermissionReq.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onXXPermissionsGranted(all: Boolean, perms: List<String>) {
        Log.i(TAG, "onXXPermissionsGranted all=$all perms=$perms")
    }

    override fun onXXPermissionsDenied(never: Boolean, perms: List<String>) {
        Log.i(TAG, "onXXPermissionsDenied never=$never perms=$perms")
    }

    fun testScopeExp(view: View) {
        startActivity(Intent(this, TestScopeCrashActivity::class.java))
    }
}
