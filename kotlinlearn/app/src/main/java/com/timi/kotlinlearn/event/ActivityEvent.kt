package com.timi.kotlinlearn.event

data class ActivityEvent(val key: String, val value: String)
