package com.timi.kotlinlearn.dialog

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.* // ktlint-disable no-wildcard-imports
import com.timi.kotlinlearn.R

/**
 *  全屏的Dialog
 *  @author lifeng.xing timi
 *  created at 2021/12/24  9:31
 */
class FullScreenDialogFragment : BaseDialogFragment() {
    val TAG = "FullScreenDialogFragment"
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // 去掉dialog的标题，需要在setContentView()之前
        this.dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return inflater.inflate(R.layout.fragment_dialog_fullscreen, container, false) // 自己的布局文件
    }

    override fun onStart() {
        super.onStart()
        val window: Window? = this.dialog?.window
        // 去掉dialog默认的padding
        // 去掉dialog默认的padding
        window?.decorView?.setPadding(0, 0, 0, 0)
        val lp: WindowManager.LayoutParams? = window?.attributes
        lp?.width = WindowManager.LayoutParams.MATCH_PARENT
        lp?.height = WindowManager.LayoutParams.MATCH_PARENT
        window?.attributes = lp
    }
    companion object {
        @JvmStatic
        fun newInstance() =
            FullScreenDialogFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
    @SuppressLint("LongLogTag")
    override fun intercept(chain: DialogChain) {
        super.intercept(chain)
        chain.activity?.supportFragmentManager?.let { show(it, "FullScreenDialogFragment") }
    }
}
