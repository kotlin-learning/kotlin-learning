package com.timi.kotlinlearn.dialog

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.view.* // ktlint-disable no-wildcard-imports
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.annotation.Nullable
import com.timi.kotlinlearn.R

/**
 *  顶部显示、全屏、宽度全屏、高度自定义、背景全透明；
 *  仿网易云音乐下载完成提示；
 *  @author lifeng.xing timi
 *  created at 2021/12/24  10:53
 */
class TopTipDialog : BaseDialogFragment() {
    private lateinit var ll: LinearLayout
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.fragment_dialog_top, container, false)
        ll = inflate.findViewById(R.id.mView)
        val params = ll.layoutParams as FrameLayout.LayoutParams
        params.topMargin = getStatusBarHeight(dialog!!.context)
        ll.layoutParams = params // 默认在最顶部，把状态栏部分留出来
        return inflate
    }
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    companion object {
        @JvmStatic
        fun newInstance() =
            TopTipDialog().apply {
                arguments = Bundle().apply {
                }
            }
    }

    override fun onStart() {
        super.onStart()
        val window: Window? = dialog!!.window
        val windowParams: WindowManager.LayoutParams? = window?.attributes
        windowParams?.dimAmount = 0.0f // Dialog外边框透明
        window?.setLayout(-1, -2) // 高度自适应，宽度全屏
        windowParams?.gravity = Gravity.TOP // 在顶部显示
        windowParams?.windowAnimations = R.style.TopDialogAnimation
        window?.attributes = windowParams
    }

    /**
     * 获取状态栏高度（单位：px）
     */
    private fun getStatusBarHeight(context: Context): Int {
        val resources: Resources = context.resources
        val resourceId: Int = resources.getIdentifier("status_bar_height", "dimen", "android")
        return if (resources.getDimensionPixelSize(resourceId) === 0) 60 else resources.getDimensionPixelSize(
            resourceId
        )
    }
    override fun intercept(chain: DialogChain) {
        super.intercept(chain)
        chain.activity?.supportFragmentManager?.let { show(it, "TopTipDialog") }
    }
}
